<?php
Calypso::AddPartialView('_addDeviceToMerchantModal',['devices'=> !empty($all_devices)? $all_devices:[]]);
Calypso::AddPartialView('_addCashierModal');
Calypso::AddPartialView('notices');

if(!empty($all_devices)){
    //var_dump($all_devices);
}
if(!empty($cashiers)){
    //var_dump($cashiers);
}
if(!empty($devices)){
    //var_dump($devices);
}
?>
<div class="btn-group-xs">
<?php
    if(!empty($merchant)): ?>
    <a class="btn btn-primary btn-rounded" href="<?php echo BASE_PATH; ?>/merchant/transactions/?id=<?php echo $merchant['id'];?>">
    View Transactions
    </a>
    <?php endif; ?>
    <a onclick="javascript:location.reload();" class="btn btn-default pull-right">Refresh Page
    </a>
    <button data-toggle="modal" data-target="#add_cashier" class="btn btn-warning pull-right">Add Cashier
    </button>
    <a  data-toggle="modal" data-target="#add_device_merchant" class="btn btn-success pull-right">Assign Device
    </a>
</div>
<br/>
<div class="row">
    <div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title row">

                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Merchant Detail</div>
                        <div class="panel-body">
                            <?php

                            if(!empty($merchant)): ?>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Merchant name</th>
                                        <th><?= $merchant['name']; ?></th>
                                    </tr>
                                    <tr>
                                        <th>Merchant Phone Number</th>
                                        <th><?= $merchant['phone_number']; ?></th>
                                    </tr>
                                    <tr>
                                        <th>Merchant Address</th>
                                        <th><?= $merchant['address']; ?></th>
                                    </tr>

                                    <tr>
                                        <th>Merchant bank</th>
                                        <th><?= $merchant['bank']; ?></th>
                                    </tr>
                                    <tr>
                                        <th>Merchant Account Name</th>
                                        <th><?= $merchant['account_name']; ?></th>
                                    </tr>
                                    <tr>
                                        <th>Merchant Account Number</th>
                                        <th><?= $merchant['account_number']; ?></th>
                                    </tr>
                                    <tr>
                                        <th>Merchant Charge</th>
                                        <th>N<?= $merchant['charge']; ?></th>
                                    </tr>
                                    <tr>
                                        <th>Settings</th>
                                        <th>
                                            <ul class="list-inline">
                                                <li>
                                                <a  class="btn btn-success">
                                                Edit</a></li>

                                               <a  class="btn btn-danger">
                                                Suspend</a></li>
                                            </ul>
                                        </th
                                    </tr>

                                    </thead>
                                </table>
                            <?php endif; ?>
                        </div>
                    </div>


                </div>

                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Devices</div>
                        <div class="panel-body">
                            <p>&nbsp;</p>
                            <table id="__devices" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Device Code</th>
                                    <th>Device Address</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php  if(!empty($devices['data'])):
                                    $i = 0;
                                    foreach($devices['data'] as $device):
                                        ?>
                                        <tr>
                                            <td><?=  ++$i; ?></td>
                                            <td><?=  $device['device_code']; ?></td>
                                            <td><?=  $device['address']; ?></td>
                                        </tr>
                                        <?php
                                    endforeach;
                                endif; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <div class="ibox-title">
                <div class="panel panel-primary">
                    <div class="panel-heading">Cashiers</div>
                    <div class="panel-body">
                        <p>&nbsp;</p>
                        <div>
                            <?php
                            if(!empty($cashiers)) {
                                ?>
                                <table class="table table-bordered dataTable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Email</th>
                                        <th>Phone Number</th>
                                        <th>Date Created</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i = 0;
                                    foreach($cashiers['data'] as $item){

                                        ?>
                                        <tr>
                                            <td><?=  ++$i;?></td>
                                            <td><?= $item['email']; ?></td>
                                            <td><?= $item['telephone']; ?></td>
                                            <td><?= $item['created_time']; ?></td>
                                        </tr>

                                        <?php
                                    }
                                    ?></tbody>
                                </table>
                                <?php

                            }
                            ?>

                        </div>
                    </div>
                </div>


            </div>
        </div>

    </div>
</div>
