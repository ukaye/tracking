<?php
Calypso::AddPartialView('notices');


?>
<script type="text/javascript" src="<?php echo BASE_PATH; ?>/js/jquery.dataTables.min.js"></script>
<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo BASE_PATH; ?>/home/merchants">Merchants</a>
            </li>
            <li class="active">
                <strong>Merchant Sync.</strong>
            </li>
        </ol>
    </div>
</div>
<h1>
<?php
if(Calypso::getInstance()->isAdmin()) {
    ?> 
    <div class="top_btns">  
        <a href="<?php echo BASE_PATH; ?>/merchant/details<?php echo $merchant['id'] ?>"><i class="fa fa-refresh"></i></a> 
        <a class="btn btn-success" href="<?php echo BASE_PATH; ?>/home/merchantdetail/<?php echo $merchant['id'] ?>" class="btn btn-primary">Merchant Detail</a>
        <a class="btn btn-warning" href="<?php echo BASE_PATH; ?>/home/merchant_transactions/<?php echo $merchant['id'] ?>" class="btn btn-primary">View all Transactions</a>
    </div>
    <?php
}
?>
</h1>
<div class="row" style="display: block;">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><?php echo ($merchant['name']); ?>&nbsp;Synchronisations</h5>
        <div class="ibox-tools">
            <span class="label label-primary">Last Updated:&nbsp;<span id="date"></span><script>document.getElementById("date").innerHTML = Date();</script></span>
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
    <div id="editable_wrapper" class="dataTables_wrapper form-inline">
    <table class="table table-striped table-bordered table-hover  dataTable" id="editable" role="grid" aria-describedby="editable_info">
    <thead>
    <tr role="row">
        <th tabindex="0" rowspan="1" colspan="1">#</th>
        <th tabindex="0" rowspan="1" colspan="1">Sync Date</th>
        <th tabindex="0" rowspan="1" colspan="1">Total Amount</th>
        <th tabindex="0" rowspan="1" colspan="1">No. of Transactions</th>
        <th tabindex="0" rowspan="1" colspan="1">Payment Status</th>
        <th tabindex="0" rowspan="1" colspan="1">Actions</th>
    </tr>
    </thead>
    <tbody>
            <?php
            if(!empty($sync_transactions)) {
                $x = 0;
                foreach($sync_transactions as $transaction){

                ?>
                <tr>
                  <td><?php echo ++$x; ?></td>
                    <td>
                        <?= $transaction['created_time']?>
                    </td>
                     <td>
                         <?= $transaction['total_amount']?>
                    </td>
                    <td>
                         <?= $transaction['no_of_transaction']?>
                    </td>
                   
                    <td>
                         <?= $transaction['status']?>
                    </td>
                    
                    <td>
                        <a href="#" class="btn btn-primary">Pay-Out</a>
                        <a href="<?php echo BASE_PATH; ?>/merchant/transactions/<?php echo $merchant['id']. '/'.$transaction['id']  ?>" class="btn btn-primary">View All</a>
                    </td>
                </tr>
                <?php

                }
            }
            ?>
            </tbody>
    </table>
</div>

    </div>
    </div>
    </div>
    </div>

<script type="text/javascript">
    $(document).ready(function(){
        $('table').DataTable();
    });
</script>
