<?php
Calypso::AddPartialView('notices');


?>
<script type="text/javascript" src="<?php echo BASE_PATH; ?>/js/jquery.dataTables.min.js"></script>
<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo BASE_PATH; ?>/home/merchants">Merchants</a>
            </li>
            <li>
                <a href="<?php echo BASE_PATH; ?>/home/merchant_sync/<?php echo $merchant['id'] ?>">Merchant Sync.</a>
            </li>
            <li class="active">
                <strong>Merchant Transactions</strong>
            </li>
        </ol>
    </div>
</div>
<h1>Merchant Transactions
    <a href="<?php echo BASE_PATH; ?>/home/merchant_transactions/<?php echo $merchant['id'] ?>" class="refresh">
        <i class="fa fa-refresh"></i>
    </a>
</h1>
<?php
if(Calypso::getInstance()->isAdmin()) {
    ?>    
    <a class="btn btn-success pull-right evt-margin-top-x50-neg" href="<?php echo BASE_PATH; ?>/home/merchantdetail/<?php echo $merchant['id'] ?>" class="btn btn-primary">Merchant Detail</a>
    <?php
}
?>
<div class="row" style="display: block;">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><?php echo ($merchant['name']); ?>&nbsp;Transactions</h5>
        <div class="ibox-tools">
            <span class="label label-primary">Last Updated:&nbsp;<span id="date"></span><script>document.getElementById("date").innerHTML = Date();</script></span>
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
    <div id="editable_wrapper" class="dataTables_wrapper form-inline">
    <table class="table table-striped table-bordered table-hover  dataTable" id="editable" role="grid" aria-describedby="editable_info">
    <thead>
    <tr role="row">
        <th tabindex="0" rowspan="1" colspan="1">#</th>
        <th tabindex="0" rowspan="1" colspan="1">Card Serial</th>
        <th tabindex="0" rowspan="1" colspan="1">Amount</th>
        <th tabindex="0" rowspan="1" colspan="1"> Card Balance</th>
        <th tabindex="0" rowspan="1" colspan="1">Type</th>
        
        <th tabindex="0" rowspan="1" colspan="1">Trans.Date</th>
        <th tabindex="0" rowspan="1" colspan="1">Sync. Date</th>
        <th tabindex="0" rowspan="1" colspan="1">Actions</th>
    </tr>
    </thead>
    <tbody>
            <?php
            if(!empty($transactions)){
                $x = 0;
                foreach($transactions as $transaction){

                ?>
                <tr>
                    <td><?php echo ++$x; ?></td>
                    <td><?php echo $transaction['serial_number'] ?></td>
                    <td><?php echo $transaction['amount'] ?></td>
                    <td><?php echo $transaction['card']['balance'] ?></td>
                    <td>
                        
                        <?php
                            $type = $transaction['device_type_id'];

                            if ($type == 1){
                                echo "credit";
                            }
                            else if ($type == 2){
                                echo "debit";
                            }


                        ?>
                    </td>
                     <td><?php echo $transaction['created_time'] ?></td>
                     <td><?php echo $transaction['sync_time'] ?></td>
                
                    
                    
                    <td>
                            <?php
                                if($transaction['card']['customer_id'] != null){
                                    echo ' <a href="#" class="btn btn-primary">View Customer</a>';
                                }
                                else
                                {
                                    echo "Card Not Registered";
                                }

                            ?>
                   </td>
                </tr>
                <?php

                }
            }
            ?>
            </tbody>
    </table>
</div>

    </div>
    </div>
    </div>
    </div>

<script type="text/javascript">
    $(document).ready(function(){
        $('table').DataTable();
    });
</script>
