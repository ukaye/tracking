<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title><?php echo empty($this->_title)?'Payment Platform':$this->_title ?></title>
    <!-- <link rel="shortcut icon" href="<?php echo BASE_PATH; ?>/img/favicon.png" type="image/x-icon"> -->
    <link href="<?php echo BASE_PATH; ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo BASE_PATH; ?>/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo BASE_PATH; ?>/css/bootstrap-select.min.css" rel="stylesheet">

    <link href="<?php echo BASE_PATH; ?>/css/datepicker.css" rel="stylesheet">
    <link href="<?php echo BASE_PATH; ?>/css/jquery.mCustomScrollbar.css" rel="stylesheet">
    <link href="<?php echo BASE_PATH; ?>/css/fullcalendar.min.css" rel="stylesheet">
    <link href="<?php echo BASE_PATH; ?>/css/mentorarena.css" rel="stylesheet">
 
    <link href="<?php echo BASE_PATH; ?>/css/animate.css" rel="stylesheet">
    <link href="<?php echo BASE_PATH; ?>/css/style.css" rel="stylesheet">
    <link href="<?php echo BASE_PATH; ?>/css/custom.css" rel="stylesheet">
    <!-- Data Tables -->
    <link href="<?php echo BASE_PATH; ?>/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?php echo BASE_PATH; ?>/css/plugins/dataTables/dataTables.responsive.css" rel="stylesheet">
    <link href="<?php echo BASE_PATH; ?>/css/plugins/dataTables/dataTables.tableTools.min.css" rel="stylesheet">

    <script type="text/javascript" src="<?php echo BASE_PATH; ?>/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo BASE_PATH; ?>/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo BASE_PATH; ?>/js/bootstrap-select.min.js"></script>
    <!--<script type="text/javascript" src="/js/ractive.js"></script>-->
    <script type="text/javascript" src="<?php echo BASE_PATH; ?>/js/eventive.js"></script>
</head>
<body>
<?php if(Calypso::getInstance()->isLoggedIn()) {

$session = Calypso::getInstance()->session('user');
Calypso::AddPartialView('_passwordModal');
?>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span class="animated bounceIn">

                            <!-- <img alt="image" src="<?php echo BASE_PATH; ?>/img/sanwologo.png" width="150px" height="50px;"/> -->
                            <div style="width:150px; height:50px;"></div>
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <strong><h2><?php echo empty($session['issuer'])?'Sanwo Client': strtoupper($session['issuer']['name']); ?></h2></strong>
                             <!--<span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo $session['email']; ?></strong>-->
                             </span></span> 
                        </a>
                    </div>
                    <div class="logo-element">
                        SHQ
                    </div>
                </li>
                <li class="active">
                    <a href="#"><i class="fa fa-home"></i> <span class="nav-label">Home</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level" bs-active-link>
                        <li class="active"><a href="<?php echo BASE_PATH; ?>/home"><span class="fa fa-sm fa-th"></span>&nbsp;Dashboard</a></li>
                          <li><a href="<?php echo BASE_PATH; ?>/merchant"><span class="fa fa-user-plus"></span>&nbsp;Merchants</a></li>
                        <li><a href="<?php echo BASE_PATH; ?>/Agents"><span class="fa fa-user"></span>&nbsp;Agents</a></li>
                        <li><a href="<?php echo BASE_PATH; ?>/customers"><span class="fa fa-users"></span>&nbsp;Customers</a></li>

                        <li><a href="<?php echo BASE_PATH; ?>/device"><span class="fa fa-lg fa-mobile-phone"></span>&nbsp;Devices</a></li>
                      
                        <li><a href="<?php echo BASE_PATH; ?>/cards"><span class="fa fa-sm fa-credit-card"></span>&nbsp;<?php echo empty($session['issuer'])?'Sanwo': ucwords($session['issuer']['name']); ?> Cards</a></li>
                        
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-th-large fa-bank"></i> <span class="nav-label">Finance</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="<?php echo BASE_PATH; ?>/finance"><span class="glyphicon glyphicon-book"></span>&nbsp;Contribution Record</a></li>
                        <li><a href="<?php echo BASE_PATH; ?>/finance"><span class="glyphicon glyphicon-bookmark"></span>&nbsp;Settlement History</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-th-large fa-users"></i> <span class="nav-label">Administration</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="<?php echo BASE_PATH; ?>/management/staff"><span class="glyphicon glyphicon-user"></span>&nbsp; Staff Management</a></li>
                        <li><a href="<?php echo BASE_PATH; ?>/rolesmanagement/"><span class="glyphicon glyphicon-user"></span>&nbsp; Role Management</a></li>
                        <li><a href="<?php echo BASE_PATH; ?>/management/"><span class="glyphicon glyphicon-tint"></span>&nbsp;Sanwo Credit Mgt</a></li>
                        <li><a href="<?php echo BASE_PATH; ?>/management/"><span class="glyphicon glyphicon-screenshot"></span>&nbsp;Sanwo Credit Allc</a></li>
                        <li><a href="<?php echo BASE_PATH; ?>/management/"><span class="glyphicon glyphicon-stats"></span>&nbsp;SMS Management</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-th-large fa-briefcase"></i> <span class="nav-label">Contribution Mgt</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="<?php echo BASE_PATH; ?>/management/contributionlifecycle"><span class="glyphicon glyphicon-retweet"></span>&nbsp;Contribution Cycle Management</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <span class="navbar-brand"><?php echo empty($session['issuer'])?'Sanwo Client': strtoupper($session['issuer']['name']); ?></span>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                            <a href="#">Logged in as <?php echo $session['email']; ?><span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#"  data-toggle="modal"  data-target="#reset_password_modal" role="dialog">Reset Password</a></li>
                                <li><a href="<?php echo BASE_PATH; ?>/home/settings">Settings</a></li>
                                <li><a href="<?php echo BASE_PATH; ?>/home/logout">Logout</a></li>
                            </ul>
                        </li>
            </ul>

        </nav>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">

                <?php
                if(Calypso::getInstance()->isAdmin()) {
                    ?>
<?php
                }
                ?>
    <?php

}else{
?>

<?php } ?>