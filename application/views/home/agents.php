<script type="text/javascript" src="<?php echo BASE_PATH; ?>/js/jquery.dataTables.min.js"></script>
<h1>Registered Agents
<a href="<?php echo BASE_PATH; ?>/home/agents" class="refresh">
        <i class="fa fa-refresh"></i>
    </a>
</h1>
<?php

Calypso::AddPartialView('_addAgentModal.php');
Calypso::AddPartialView('notices');
if(Calypso::getInstance()->isAdmin()) {
    ?>
    
    <button type="button" class="btn btn-success pull-right evt-margin-top-x50-neg" data-toggle="modal"
            data-target="#add_agent">Add New Agent
    </button>
    <?php
}
?>



<div  class="modal fade" id="add_agent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">New Agent</h4>
            </div>
            <div class="modal-body">
                <p>
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <fieldset class="">
                        <div class="form-group">
                            <label for="inputEmail" class="col-lg-2 control-label">Email</label>
                            <div class="col-lg-10">
                                <input name="email" type="email" class="form-control" id="" placeholder="email address">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-lg-2 control-label">Password</label>
                            <div class="col-lg-10">
                                <input name="password" type="password" class="form-control" id="" placeholder="password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2" for="firstname">Firstname</label>
                           <div class="col-lg-10">
                                <input name="firstname" type="text" class="form-control" id="" placeholder="firstname">
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="control-label col-lg-2" for="lastname">Lastname</label>
                           <div class="col-lg-10">
                                <input name="lastname" type="text" class="form-control" id="" placeholder="lastname">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2" for="middlename">Middlename</label>
                           <div class="col-lg-10">
                                <input name="middle" type="text" class="form-control" id="" placeholder="middlename">
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="textArea" class="col-lg-2 control-label">Address</label>
                            <div class="col-lg-10">
                                <textarea class="form-control" rows="3" id="address" name="address"></textarea>

                            </div>
                        </div>
                       
                         <div class="form-group">
                            <label class="control-label col-lg-2" for="commission">Commission</label>
                           <div class="col-lg-10">
                                <input name="commission" type="text" class="form-control" id="" placeholder="commission">
                            </div>
                        </div>
                       
                        
                       
                        
                        <div class="form-group">
                            <label for="inputEmail" class="col-lg-2 control-label">Telephone</label>
                            <div class="col-lg-10">
                                <input name="telephone" type="text" class="form-control" id="" placeholder="Telephone">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2" for="gender">Gender</label>
                            <div class="col-lg-10">
                                <select id="gender" class="form-control" name="gender">
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                  
                                </select>
                                <p class="help-block help-block-error"></p>
                            </div>
                        </div>
                        
                        <br/>
                        <br/>
                        <div class="form-group">
                            <div class="col-lg-10 col-lg-offset-2">
                                <button type="reset" class="btn btn-default">Cancel</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="row" style="display: block;">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>List of agents with profile</h5>

        <div class="ibox-tools">
            <span class="label label-primary">Last Updated:&nbsp;<span id="date"></span><script>document.getElementById("date").innerHTML = Date();</script></span>
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
    <div id="editable_wrapper" class="dataTables_wrapper form-inline">

    <table class="table table-striped table-bordered table-hover  dataTable" id="editable" role="grid" aria-describedby="editable_info">
    <thead>
    <tr role="row">
        <th tabindex="0" rowspan="1" colspan="1" style="width: 10px;">#</th>
        <th tabindex="0" rowspan="1" colspan="1" style="width: 130px;">Name</th>
        <th tabindex="0" rowspan="1" colspan="1" style="width: 170px;">Email</th>
        <th tabindex="0" rowspan="1" colspan="1" style="width: 120px;">Phone</th>
        <th tabindex="0" rowspan="1" colspan="1" style="width: 150px;">Device Credit</th>
        <th tabindex="0" rowspan="1" colspan="1" style="width: 250px;">Address</th>
        <th tabindex="0" rowspan="1" colspan="1" style="width: 120px;">Action</th>
    </tr>
    </thead>
    <tbody> 
    <?php
    $noProfileAgents = []; $csv_link = '';
    if(!empty($agents)) {
        $x = 0;
        foreach($agents as $agent){
            if(!empty($agent['profile']) && !empty($agent['profile']['address'])){
        ?>            
    <tr role="row">
        <td><?php echo ++$x; ?></td>
        <td><?php echo ucwords($agent['profile']['firstname']).' '.ucwords($agent['profile']['middlename']).' '.strtoupper($agent['profile']['lastname']) ?></td>
        <td><?php echo $agent['email']; ?></td>
        <td><?php echo $agent['telephone'] ?></td>
        <!-- <td><?php echo $agent['profile']['gender']== '1'? 'M':'F'; ?></td> -->
        <td><?php echo $agent['agent_info']['unpaid_credit']; ?></td>
        <!-- <td><?php echo $agent['created_date'] ?></td> -->
        <td><?php echo $agent['profile']['address'] ?></td>
        <td>
            <a class="btn btn-link btn-xs" href="<?php echo BASE_PATH; ?>/home/agentcollectionhistory/<?php echo $agent['id'] ?>/<?php echo base64_encode(ucwords($agent['profile']['firstname']).' '.ucwords($agent['profile']['middlename']).' '.strtoupper($agent['profile']['lastname'])); ?>">Transactions</a>
                        <a class="btn btn-link btn-xs" href="<?php echo BASE_PATH; ?>/home/agentcredithistory/<?php echo $agent['id'] ?>/">Credit History</a>
                    
        </td>
    </tr>
    <?php
        }else{
            $csv = join(',', [$agent['email'],$agent['telephone']]);
            $csv_link.= $csv.'\n\r';
            array_push($noProfileAgents, $agent);
        }
    }
}
?>

    </tbody>
    </table>
</div>

    </div>
    </div>
    </div>
    </div>
    <div class="row" style="display: block;">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>List of agents without profile</h5>
        <div class="ibox-tools">
            <span class="label label-primary">Last Updated:&nbsp;<span id="date"></span><script>document.getElementById("date").innerHTML = Date();</script></span>
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
    <div id="editable_wrapper" class="dataTables_wrapper form-inline">
<?php
        if(!empty($noProfileAgents)) {
            ?>
            <a class="btn btn-sm btn-default pull-right hidden" download="NoProfile.csv" href="data:text/csv;charset=utf-8,<?php echo urlencode($csv_link); ?>">Download</a>
            <?php
        }
        ?>
    <table class="table table-striped table-bordered table-hover  dataTable" id="editable" role="grid" aria-describedby="editable_info">
    <thead>
    <tr role="row">
        <th tabindex="0" rowspan="1" colspan="1">#</th>
        <th tabindex="0" rowspan="1" colspan="1">Email</th>
        <th tabindex="0" rowspan="1" colspan="1">Phone</th>
        <th tabindex="0" rowspan="1" colspan="1">Registration Date</th>
    </tr>
    </thead>
    <tbody> 
    <?php
            if(!empty($agents)) {
                $x = 0;
                foreach($noProfileAgents as $agent){
                        ?>
                        <tr>
                            <td><?php echo ++$x; ?></td>
                            <td><?php echo $agent['email']; ?></td>
                            <td><?php echo $agent['telephone'] ?></td>
                            <td><?php echo $agent['created_date'] ?></td>
                        </tr>
                        <?php
                }
            }
            ?>

    </tbody>
    </table>
</div>

    </div>
    </div>
    </div>
    </div>
<!-- <script type="text/javascript">
    $(document).ready(function(){
        $('table').DataTable();
    });
</script> -->
