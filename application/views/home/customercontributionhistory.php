<?php
if(empty($collections))
{
    Calypso::getInstance()->setFlashSuccessMsg('Report Not Available This Time. Please try again later.');
    Calypso::AddPartialView('notices');
}else{

    ?>
    <!--<script src="http://code.highcharts.com/highcharts.js"></script>
    <script src="http://code.highcharts.com/modules/exporting.js"></script>-->
    <script type="text/javascript" src="<?php echo BASE_PATH; ?>/js/jquery.dataTables.min.js"></script>

    <!--<div class="row">
        <div class="col-md-7">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Customer's Profile</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <tbody>
                        <?php
/*                        if(!empty($profile)):
                            */?>
                            <tr>
                                <td><strong>Email</strong></td>
                                <td><?php /*echo $profile['owner']['email']; */?></td>
                            </tr>
                            <tr>
                                <td><strong>Telephone</strong></td>
                                <td><?php /*echo $profile['owner']['telephone']; */?></td>
                            </tr>
                            <tr>
                                <td><strong>Registration Date</strong></td>
                                <td><?php /*echo $profile['owner']['created_date']; */?></td>
                            </tr>
                            <tr>
                                <td><strong>Last Modification Date</strong></td>
                                <td><?php /*echo $profile['owner']['modified_date']; */?></td>
                            </tr>
                            <?php
/*                            if(!empty($profile['owner']['profile']) && !empty($profile['owner']['profile']['firstname']) && !empty($profile['owner']['profile']['middlename'])):
                                */?>
                                <tr>
                                    <td><strong>Full Name</strong></td>
                                    <td><?php /*echo $profile['owner']['profile']['firstname'].' '.$profile['owner']['profile']['middlename'].' '.$profile['owner']['profile']['lastname']; */?></td>
                                </tr>
                                <?php
/*                            endif;

                            if(!empty($profile['owner']['profile'] && !empty($profile['owner']['profile']['address']))):
                                */?>
                                <tr>
                                    <td><strong>Address</strong></td>
                                    <td><?php /*echo $profile['owner']['profile']['address']; */?></td>
                                </tr>
                                <?php
/*                            endif;

                        endif;
                        */?>

                        </tbody>
                    </table>
                    <button class="btn btn-sm pull-left btn-success">Go Back</button>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title">Card Detail</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <tbody>
                        <?php
/*                        if(!empty($profile)):
                            */?>
                            <tr>
                                <td><strong>Card Serial Number</strong></td>
                                <td><?php /*echo $profile['serial_number']; */?></td>
                            </tr>
                            <tr>
                                <td><strong>Reference Number</strong></td>
                                <td><?php /*echo $profile['ref_number']; */?></td>
                            </tr>
                            <tr>
                                <td><strong>Batch Number</strong></td>
                                <td><?php /*echo $profile['batch_number']; */?></td>
                            </tr>
                            <tr>
                                <td><strong>Expiry Date</strong></td>
                                <td><?php /*echo $profile['expiry_date']; */?></td>
                            </tr>
                            <tr>
                                <td><strong>Card Version</strong></td>
                                <td><?php /*echo 'LFT-'.$profile['version']; */?></td>
                            </tr>
                            <tr>
                                <td><strong>Created Date</strong></td>
                                <td><?php /*echo $profile['created_time']; */?></td>
                            </tr>
                            <tr>
                                <td><strong>Last Modified Date</strong></td>
                                <td><?php /*echo $profile['modified_time']; */?></td>
                            </tr>
                            <tr>
                                <td><strong>Card Status</strong></td>
                                <td><?php /*echo \SanwoPHPAdapter\Globals\ServiceConstant::getStatus($profile['status']); */?></td>
                            </tr>

                            <?php
/*
                        endif;
                        */?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>-->
    <!--<div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Customer Contribution Behaviour</h3>
                </div>
                <div class="panel-body">
                    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>

    </div>-->
    <div class="">
        <legend>Collection History</legend>
        <table id="datatable" class="table table-striped table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>Card Serial Number</th>
                <th>Transaction Type</th>
                <th>Transaction Ref</th>
                <th>Amount</th>
                <th>Card Balance</th>
                <th>Collection Date</th>
                <th>Sync Date</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(!empty($collections)) {
                $x = 0;
                foreach($collections as $collection){
                    ?>
                    <tr>
                        <td><?php echo ++$x; ?></td>
                        <td><?php echo $collection['serial_number']; ?></td>
                        <td><?php echo $collection['type'] == '1'?'Credit':'Debit'; ?></td>
                        <td><?php echo $collection['transaction_code']; ?></td>
                        <td><?php echo $collection['amount']; ?></td>
                        <td><?php echo $collection['card_balance']; ?></td>
                        <td><?php echo $collection['created_time']; ?></td>
                        <td><?php echo $collection['sync_time']; ?></td>
                        <td><?php echo \SanwoPHPAdapter\Globals\ServiceConstant::getStatus($collection['status']); ?></td>
                    </tr>
                    <?php
                }
            }
            ?>
            </tbody>
        </table>
    </div>

    <?php
}

?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
<!--<script type="text/javascript">
    $(function () {
        $('#container').highcharts({
            title: {
                text: 'Monthly Average Collections',
                x: -20 //center
            },
            subtitle: {
                text: 'Collection made and synchronized by Agent',
                x: -20
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                title: {
                    text: 'Contributions Collected (N)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valuePrefix: 'N'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: '2015',
                data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
            }, {
                name: '2014',
                data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
            }, {
                name: '2013',
                data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
            }, {
                name: '2012',
                data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
            }]
        });
    });
</script>
-->