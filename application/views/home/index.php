<?php
/**
 * Created by PhpStorm.
 * User: CodeBeast
 * Date: 10/24/2015
 * Time: 12:25 AM
 */

if(!$isLoggedIn){
    Calypso::AddPartialView('_login');
}else{
    ?>
    <h1>Summary</h1>
    <?php
    //var_dump($agents[0]);
    ?>
    <div class="row">
      <div class="col-lg-8">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Line Chart showing frequency of sales
                    <small>December 2015</small>
                </h5>
                <div ibox-tools=""></div>
            </div>
            <div class="ibox-content">
                <div>
                    <canvas id="lineChart" height="200" width="535" style="width: 535px; height: 200px;"></canvas>
                </div>
            </div>
        </div>
      </div>
    <div class="col-lg-4">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <span class="label label-primary pull-right">2015</span>
                            <h5>Card Allocation Usage</h5>
                        </div>
                        <div class="ibox-content">
                            <h1 class="no-margins">22 285,400</h1>
                            <div class="stat-percent font-bold text-navy"><i class="fa fa-level-up"></i></div>
                            <small>Assigned</small>
                        </div>
                    </div>
            </div>
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-info pull-right">2015</span>
                        <h5>SMS Usage</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">60 420,600</h1>
                        <div class="stat-percent font-bold text-info"><i class="fa fa-level-up"></i></div>
                        <small>Units Used</small>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="widget style1 navy-bg">
            <div class="row">
                <div class="col-xs-4">
                    <i class="fa fa-hand-o-right fa-5x"></i>
                </div>
                <div class="col-xs-8 text-right">
                    <em> Total number of Agents </em>
                    <h2>
                        <?php
                    $count = count($agents);
                    if($count > 0){
                        if($count > 1) {echo $count.' Registered Agents';}
                        else {echo $count.' Registered Agent(s)';}
                    }else{
                        echo 'No Registered Agent(s)';
                    }
                    ?>
                    </h2>
                </div>
            </div>
        </div>
        </div>
        <div class="col-md-4">
            <div class="widget style1 lazur-bg">
            <div class="row">
                <div class="col-xs-4">
                    <i class="fa fa-user fa-5x"></i>
                </div>
                <div class="col-xs-8 text-right">
                    <em> Total number of Members</em>
                    <h2>
                        <?php
                    $count = count($agents);
                    if($count > 0){
                        if($count > 1) {echo $count.' Registered Member(s)';}
                        else {echo $count.' Registered Member(s)';}
                    }else{
                        echo 'No Registered Members';
                    }
                    ?>
                    </h2>
                </div>
            </div>
        </div>
        </div>
        <div class="col-md-4">
            <div class="widget style1 yellow-bg">
            <div class="row">
                <div class="col-xs-4">
                    <i class="fa fa-phone fa-5x"></i>
                </div>
                <div class="col-xs-8 text-right">
                    <em> Total number of Devices</em>
                    <h2>46,760</h2>
                </div>
            </div>
        </div>
        </div>
    </div>
    <!-- <div class="row hidden">
        <div class="col-md-4">
            <div class="widget style1 navy-bg">
            <div class="row">
                <div class="col-xs-4">
                    <i class="fa fa-hand-o-right fa-5x"></i>
                </div>
                <div class="col-xs-8 text-right">
                    <span> Total number of Agents </span>
                    <h2>
                        <?php
                    $count = count($agents);
                    if($count > 0){
                        if($count > 1) {echo $count.' Registered Agents';}
                        else {echo $count.' Registered Agent';}
                    }else{
                        echo 'No Registered Agents';
                    }
                    ?>
                    </h2>
                </div>
            </div>
        </div>
        </div>
        <div class="col-md-4">
            <div class="widget style1 lazur-bg">
            <div class="row">
                <div class="col-xs-4">
                    <i class="fa fa-user fa-5x"></i>
                </div>
                <div class="col-xs-8 text-right">
                    <span> Total number of Members</span>
                    <h2>
                        <?php
                    $count = count($agents);
                    if($count > 0){
                        if($count > 1) {echo $count.' Registered Agents';}
                        else {echo $count.' Registered Agent';}
                    }else{
                        echo 'No Registered Agents';
                    }
                    ?>
                    </h2>
                </div>
            </div>
        </div>
        </div>
        <div class="col-md-4">
            <div class="widget style1 yellow-bg">
            <div class="row">
                <div class="col-xs-4">
                    <i class="fa fa-phone fa-5x"></i>
                </div>
                <div class="col-xs-8 text-right">
                    <span> Total number of Devices</span>
                    <h2>46,760</h2>
                </div>
            </div>
        </div>
        </div>
    </div> -->

<?php
}