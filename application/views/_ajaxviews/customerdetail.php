<?php
Calypso::AddPartialView('notices');

if(!empty($data)){
?>
    <table class="table table-bordered">
        <tbody>
        <?php
        if(!empty($data['profile']) && !empty($data['profile']['firstname'])):
        ?>
        <tr>
            <td><strong>Full Name</strong></td>
            <td><?php echo ucwords($data['profile']['firstname']).' '.ucwords($data['profile']['lastname']);
                if (!empty($data['profile']['middlename'])){
                    echo ' '.ucwords($data['profile']['middlename']);
                }
                ?></td>
        </tr>
        <?php
        endif;

        if(!empty($data['profile']) && !empty($data['profile']['address']) && !empty($data['profile']['address'])):
        ?>
        <tr>
            <td><strong>Address</strong></td>
            <td><?php echo $data['profile']['address']; ?></td>
        </tr>
           <tr>
            <td><strong>Gender</strong></td>
            <td><?php echo $data['profile']['gender']=='1'?'MALE':'FEMALE'; ?></td>
        </tr>

        <?php
        endif;
        ?>

        <tr>
                <td><strong>Email</strong></td>
                <td><?php echo $data['email']; ?></td>
            </tr>
            <tr>
                <td><strong>Telephone</strong></td>
                <td><?php echo $data['telephone']; ?></td>
            </tr>
            <tr>
                <td><strong>Registration Date</strong></td>
                <td><?php echo $data['created_date']; ?></td>
            </tr>
            <tr>
                <td><strong>Last Modification Date</strong></td>
                <td><?php echo $data['modified_date']; ?></td>
            </tr>
        </tbody>
    </table>
<?php
}else{
    Calypso::getInstance()->setFlashErrorMsg("Details not available.");
}
?>