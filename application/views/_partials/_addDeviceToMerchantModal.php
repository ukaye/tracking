<div  class="modal fade" id="add_device_merchant" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">Add Device to Merchant</h4>
            </div>
            <div class="modal-body">
                <p>

                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <fieldset class="">
                        <input name="action_type" value="add_device_to_merchant" type="hidden">
                        <div class="form-group">
                            <label for="inputEmail" class="col-lg-2 control-label">Device Code</label>
                            <div class="col-lg-10">
                                <select name="device_code" class="form-control">
                                    <option value="">Select a device...</option>
                                    <?php
                                    if(!empty($devices) && !empty($devices['data'])){
                                        foreach($devices['data'] as $device){
                                            if($device['status']== \SanwoPHPAdapter\Globals\ServiceConstant::STATUS_DEVICE_UNASSIGNED &&
                                                $device['device_type_id']== \SanwoPHPAdapter\Globals\ServiceConstant::DEVICE_TYPE_CASHIER) {
                                                ?>
                                                <option value="<?php echo $device['id']; ?>"><?php echo $device['device_code']; ?></option>
                                                <?php
                                            }
                                    }}
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="textArea" class="col-lg-2 control-label">Address/Site of Use</label>
                            <div class="col-lg-10">
                                <textarea class="form-control" rows="3" id="address" name="address"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <br/>
                        <br/>
                        <div class="form-group">
                            <div class="col-lg-10 col-lg-offset-2">
                                <button type="reset" class="btn btn-default">Cancel</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
                </p>
            </div>
        </div>
    </div>
</div>