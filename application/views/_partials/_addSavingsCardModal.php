<div  class="modal fade" id="add_saving_card" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">Add Withdrawal Card</h4>
            </div>
            <div class="modal-body">
                <p>
                    <form id="add_serial_no_fm" class="form-horizontal" method="post" enctype="multipart/form-data">
                        <fieldset class="">
                            <input id="sanwo_id" name="sanwo_id" value="1" type="hidden">
                            <input id="sanwo_name" name="sanwo_name" value="1" type="hidden">
                            <input name="action_type" value="add_serial_no" type="hidden">
                            <div class="form-group">
                                <label for="inputEmail" class="col-lg-2 control-label">Card Serial Number</label>
                                <div class="col-lg-10">
                                    <input class="form-control" type="text" id="saving_card_no" name="saving_card_no">
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                    <button type="button" id="save_serial_no" class="btn btn-primary">Save Record</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#save_serial_no").unbind('click').on('click', function(){
            var data = $("#add_serial_no_fm").serialize();
            $.post("/public/home/updateNumber",data, function(jresp){
                try{
                    var d = JSON.parse(jresp);
                    if(d.status=='success')
                    {
                        alert(d.message);
                        location.reload();
                    }else{
                        alert(d.message);
                    }
                }catch(e){ alert(e);}

            });
        });
        $("#add_saving_card").on("show.bs.modal",function(event){
            var button = $(event.relatedTarget) // Button that triggered the modal
            var sanwo_id = button.data('id') ;
            var sanwo_name = button.data('name') ;
            var modal = $(this)
            modal.find('#sanwo_id').val(sanwo_id);
            modal.find('#sanwo_name').val(sanwo_name)
        });
    });
    /*
    * var datastring = $("#contactForm").serialize();
     $.ajax({
     type: "POST",
     url: "your url.php",
     data: datastring,
     dataType: "json",
     success: function(data) {
     //var obj = jQuery.parseJSON(data); if the dataType is not specified as json uncomment this
     // do what ever you want with the server response
     },
     error: function(){
     alert('error handing here');
     }
     });
    * */
</script>
