<div  class="modal fade" id="add_customer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">New Customer</h4>
            </div>
            <div class="modal-body">
                <p>
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <div class="form-group">
                            <label for="inputEmail" class="col-lg-2 control-label">BeepPay Card Serial Number</label>
                            <div class="col-lg-10">
                                <input name="bp_serial" type="text" class="form-control" id="" placeholder="BeepPay Card Number">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="col-lg-2 control-label">Email</label>
                            <div class="col-lg-10">
                                <input name="email" type="text" class="form-control" id="inputEmail" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="col-lg-2 control-label">Telephone</label>
                            <div class="col-lg-10">
                                <input name="telephone" type="text" class="form-control" id="" placeholder="Telephone">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="col-lg-2 control-label">Firstname</label>
                            <div class="col-lg-10">
                                <input name="firstname" type="text" class="form-control" id="" placeholder="Firstname">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="col-lg-2 control-label">Lastname</label>
                            <div class="col-lg-10">
                                <input name="lastname" type="text" class="form-control" id="" placeholder="Lastname">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="col-lg-2 control-label">Middlename</label>
                            <div class="col-lg-10">
                                <input name="middlename" type="text" class="form-control" id="" placeholder="Middlename">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="textArea" class="col-lg-2 control-label">Address</label>
                            <div class="col-lg-10">
                                <textarea class="form-control" rows="3" id="address" name="address"></textarea>
                                <span class="help-block">A longer block of help text that breaks onto a new line and may extend beyond one line.</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Gender</label>
                            <div class="col-lg-10">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="gender" id="gender" value="1" checked="">
                                        Male
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="gender" id="gender" value="2">
                                        Female
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="col-lg-2 control-label">GT Card Serial</label>
                            <div class="col-lg-10">
                                <input name="gt_serial" type="text" class="form-control" id="" placeholder="Savings Card Number">
                            </div>
                        </div>
                        <br/>
                        <br/>
                        <div class="form-group">
                            <div class="col-lg-10 col-lg-offset-2">
                                <button type="reset" class="btn btn-default">Cancel</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
                </p>
            </div>
        </div>
    </div>
</div>