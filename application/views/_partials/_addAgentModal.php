
<div  class="modal fade" id="add_agent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">New Merchant</h4>
            </div>
            <div class="modal-body">
                <p>
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <fieldset class="">
                        <div class="form-group">
                            <label for="inputEmail" class="col-lg-2 control-label">Store/Merchant Name</label>
                            <div class="col-lg-10">
                                <input name="name" type="text" class="form-control" id="" placeholder="Store/Merchant Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="textArea" class="col-lg-2 control-label">Address</label>
                            <div class="col-lg-10">
                                <textarea class="form-control" rows="3" id="address" name="address"></textarea>
                                <span class="help-block">A longer block of help text that breaks onto a new line and may extend beyond one line.</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2" for="merchantbusinesstype">Merchant Business Type</label>
                            <div class="col-lg-10">
                                <select id="merchantbusinesstype" class="form-control" name="merchantbusinesstype">
                                    <option value="1">TRANSPORT</option>
                                    <option value="2">EATERY</option>
                                    <option value="3">RETAIL</option>
                                </select>
                                <p class="help-block help-block-error"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2" for="bank">Bank</label>
                            <div class="col-lg-10">
                                <select id="bank" class="form-control" name="bank">
                                    <option value="Access Bank">Access Bank</option>
                                    <option value="Citibank">Citibank</option>
                                    <option value="Diamond Bank">Diamond Bank</option>
                                    <option value="Ecobank Nigeria">Ecobank Nigeria</option>
                                    <option value="Enterprise Bank Limited">Enterprise Bank Limited</option>
                                    <option value="Fidelity Bank Nigeria">Fidelity Bank Nigeria</option>
                                    <option value="First Bank of Nigeria">First Bank of Nigeria</option>
                                    <option value="First City Monument Bank">First City Monument Bank</option>
                                    <option value="Guaranty Trust Bank">Guaranty Trust Bank</option>
                                    <option value="Heritage Bank Plc">Heritage Bank Plc</option>
                                    <option value="Keystone Bank Limited">Keystone Bank Limited</option>
                                    <option value="Mainstreet Bank Limited">Mainstreet Bank Limited</option>
                                    <option value="Rand Merchant Bank">Rand Merchant Bank</option>
                                    <option value="Savannah Bank">Savannah Bank</option>
                                    <option value="Skye Bank">Skye Bank</option>
                                    <option value="Stanbic IBTC Bank Nigeria Limited">Stanbic IBTC Bank Nigeria Limited</option>
                                    <option value="Standard Chartered Bank">Standard Chartered Bank</option>
                                    <option value="Sterling Bank">Sterling Bank</option>
                                    <option value="Union Bank of Nigeria">Union Bank of Nigeria</option>
                                    <option value="United Bank for Africa">United Bank for Africa</option>
                                    <option value="Unity Bank Plc">Unity Bank Plc</option>
                                    <option value="Wema Bank">Wema Bank</option>
                                    <option value="Zenith Bank">Zenith Bank</option>
                                </select>
                                <p class="help-block help-block-error"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2" for="account_name">Merchant Account Name</label>
                            <div class="col-lg-10">
                                <input type="text" id="account_name" class="form-control" name="account_name">

                                <p class="help-block help-block-error"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2" for="account_number">Merchant Account Number</label>
                            <div class="col-lg-10">
                                <input type="text" id="account_number" class="form-control" name="account_number">

                                <p class="help-block help-block-error"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2" for="charge_type">Merchant Charge Type</label>
                            <div class="col-lg-10">
                                <select id="charge_type" class="form-control" name="charge_type">
                                    <option value="1">Absolute</option>
                                    <option value="2">Percentage</option>
                                </select>

                                <p class="help-block help-block-error"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2" for="charge">Merchant Charge</label>
                            <div class="col-lg-10">
                                <input type="text" id="charge" class="form-control" name="charge">

                                <p class="help-block help-block-error"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="col-lg-2 control-label">Telephone</label>
                            <div class="col-lg-10">
                                <input name="telephone" type="text" class="form-control" id="" placeholder="Telephone">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="col-lg-2 control-label">Email</label>
                            <div class="col-lg-10">
                                <input name="email" type="text" class="form-control" id="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="col-lg-2 control-label">Password</label>
                            <div class="col-lg-10">
                                <input name="password" type="text" class="form-control" id="" placeholder="password">
                            </div>
                        </div>
                        <br/>
                        <br/>
                        <div class="form-group">
                            <div class="col-lg-10 col-lg-offset-2">
                                <button type="reset" class="btn btn-default">Cancel</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
                </p>
            </div>
        </div>
    </div>
</div>