<div  class="modal fade" id="add_customer_cycle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">Check Out Cycle</h4>
            </div>
            <div class="modal-body">
                <?php
                $cycles = (Calypso::getInstance()->session('cycles'));
                ?>
                <p>
                    <form id="save_cycle_form" class="form-horizontal" method="post" enctype="multipart/form-data">
                        <fieldset class="">
                            <input name="action_type" value="add_device_to_merchant" type="hidden">
                            <input id="customer_id" name="customer_id" value="" type="hidden">
                            <div class="form-group">
                                <label for="inputEmail" class="col-lg-2 control-label">Select Cycle Plan</label>
                                <div class="col-lg-10">
                                    <select name="cycle_field" id="cycle_field" class="form-control">
                                        <option> Select Cycle ...</option>
                                        <?php
                                        if(!empty($cycles) && is_array($cycles)){
                                            foreach($cycles as $cycle){

                                                    ?>
                                                    <option value="<?php echo $cycle['Cycle']['id']; ?>"><?php echo $cycle['Cycle']['name']; ?></option>
                                                    <?php

                                            }}
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail" class="col-lg-2 control-label">Charge</label>
                                <div class="col-lg-10">
                                    <input class="form-control disabled" readonly type="text" name="cycle_value" id="cycle_value">
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                    <button id="save_cycle" type="button" class="btn btn-primary">Save Record</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var cycles = <?php echo json_encode($cycles); ?>;
    function bakeCycle(){
        var baked = {};
        for(var d in cycles){
            baked[cycles[d].Cycle.id] = cycles[d];
        }
        return baked;
    }
    $(document).ready(function(){
        var bakedCycles = bakeCycle();
        $("#cycle_field").unbind('change').on('change', function(){
            if($(this).val() in bakedCycles){
                $("#cycle_value").val(bakedCycles[$(this).val()].Cycle.general_charge);
            }else{
                $("#cycle_value").val('');
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#save_cycle").unbind('click').on('click', function(){
            var data = $("#save_cycle_form").serialize();
            $.post("/public/home/updateCycle",data, function(jresp){

                try{
                    var d = JSON.parse(jresp);
                    if(d.status=='success')
                    {
                        alert(d.message);
                        location.reload();
                    }else{
                        alert(d.message);
                    }
                }catch(e){ alert(e);}

            });
        });
        $("#add_customer_cycle").on("show.bs.modal",function(event){
            var button = $(event.relatedTarget); // Button that triggered the modal
            var customer_id = button.data('id') ;
            var modal = $(this)
            modal.find('#customer_id').val(customer_id);
        });
    });

</script>