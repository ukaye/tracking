<?php
/**
 * Created by PhpStorm.
 * User: CodeBeast
 * Date: 10/24/2015
 * Time: 12:17 AM
 */
use SanwoPHPAdapter\UserAdapter;
use SanwoPHPAdapter\Util\Response;
use SanwoPHPAdapter\ResponseHandler;
use SanwoPHPAdapter\TransactionAdapter;
use SanwoPHPAdapter\AgentAdapter;
use SanwoPHPAdapter\RequestHelper;
use SanwoPHPAdapter\DeviceAdapter;
use SanwoPHPAdapter\CardAdapter;
use SanwoPHPAdapter\CustomerAdapter;
use SanwoPHPAdapter\IssuerAdapter;
use SanwoPHPAdapter\MerchantAdapter;
use SanwoPHPAdapter\CashierAdapter;
use SanwoPHPAdapter\TopupTransactionAdapter;

use SanwoPHPAdapter\Globals\ServiceConstant;

class HomeController extends VanillaController{

    private $noAuth = ['index'];
    public function beforeAction() {
        if(in_array($this->_action, $this->noAuth)) {
            return true;
        }
        parent::beforeAction();
    }

    public function index()
    {
        $postData = Calypso::getInstance()->post(true);
        if(!empty($postData)) {
            $userAdp = new UserAdapter();
            $response = $userAdp->login($postData['email'], $postData['password']);
            //var_dump($response);die;
            $response = new ResponseHandler($response);
            if($response->getStatus() == ResponseHandler::STATUS_OK){
                $data = $response->getData();
                if(/*true || !*/in_array($data['user_type_id'], [ISSUER_USER ,ISSUER_ADMIN,1, 8])) // Remove ! later
                {
                    $cycles = new Cycle();
                    Calypso::getInstance()->session('cycles', $cycles->getCycles($data['issuer']['id']));
                    Calypso::getInstance()->session('user', $data);
                    Calypso::getInstance()->registerSession();

                    $this->set('data', $data);
                }else{
                    Calypso::getInstance()->unsetSession();
                    Calypso::getInstance()->setFlashErrorMsg('Invalid Username or Password. Please try again later');
                }
            }else {
                Calypso::getInstance()->unsetSession();
                Calypso::getInstance()->setFlashErrorMsg('Invalid Username or Password. Please try again later');
            }
        }
        if(Calypso::getInstance()->isLoggedIn()){
            $data = Calypso::getInstance()->session('user');
            $agents = Calypso::getInstance()->session('agents');
            $this->set('agents', $agents);
            $this->set('data', $data);
        }
        $this->set('isLoggedIn', Calypso::getInstance()->isLoggedIn());
    }

    public function changePassword()
    {
        $passWordData = Calypso::getInstance()->post(true);
        if(!empty($passWordData)){
            if(!empty($passWordData['pwd']) && !empty($passWordData['new_pwd']))
            {
                if($passWordData['pwd'] == $passWordData['new_pwd'])
                {
                    $data = Calypso::getInstance()->session('user');
                    $userAdp = new UserAdapter($data['id'], RequestHelper::getAccessToken());
                    $trans_data = $userAdp->changePassword($passWordData['pwd']);
                    $resp = new ResponseHandler($trans_data);
                    if($resp->getStatus() == ResponseHandler::STATUS_OK)
                    {
                        Calypso::getInstance()->setFlashSuccessMsg("Password changed successfully");
                    }else{
                        Calypso::getInstance()->setFlashErrorMsg($resp->getError());
                    }
                }else{
                    Calypso::getInstance()->setFlashErrorMsg('Password must match!');
                }
            }else{
                Calypso::getInstance()->setFlashErrorMsg('Empty password fields not allowed');
            }
        }
    }

    public function logout()
    {
        /*$data = Calypso::getInstance()->session('user');
        $userAdp = new UserAdapter($data['id'], RequestHelper::getAccessToken());
        $userAdp->logout();*/
        Calypso::getInstance()->unsetSession();
        Calypso::getInstance()->setFlashErrorMsg('You have logged out successfully');
        Calypso::getInstance()->AppRedirect('home');
    }

    public function agents()
    {
        $data = Calypso::getInstance()->session('user');

        //create agent comes here
         $postData = Calypso::getInstance()->post(true);

         
         if (!empty($postData)){
            $postData['user_type_id'] = 4;
            $postData['status'] = 1;
            var_dump($postData);

            $check =$this->validateData($postData, ['firstname', 'lastname', 'email', 'telephone',
             'gender', 'user_type_id', 'status']);

            if($check){
                $agent = new UserAdapter($data['id'], RequestHelper::getAccessToken());
                $request = $agent->createAgent(
                        $postData['email'],
                        $postData['telephone'],
                        $postData['password'],
                        $postData['user_type_id'],
                        $postData['status']       
                    );

                $response = new ResponseHandler($request);
                if($response->getStatus() == ResponseHandler::STATUS_OK){
                    Calypso::getInstance()->setFlashSuccessMsg('Agent created successfully!');
                }else{

                    Calypso::getInstance()->setFlashErrorMsg($response->getError());
                }
             //   Calypso::getInstance()->unsetSession('merchants'); // Clear cached list
                //var_dump($response);

            }


         }

        $issuerAdapter = new IssuerAdapter($data['id'], RequestHelper::getAccessToken());

//        $trans_data = $issuerAdapter->getIssuerUsers(16, 4);


        //var_dump($trans_data);


        
  //      $trans_data = new ResponseHandler($trans_data);

        //var_dump($trans_data);
    


    /*    if($trans_data->getStatus() == ResponseHandler::STATUS_OK)
        {
            $agentData = $trans_data->getData();
            Calypso::getInstance()->session('agents', $agentData);
            $this->set('agents', $agentData);
        }*/
    }

    public function settings()
    {
        $data = Calypso::getInstance()->session('user');
       // $settingsAdp = new SettingsAdapter($data['id'], RequestHelper::getAccessToken());
        // $trans_data = $settingsAdp->getSettings();
        // $trans_data = new ResponseHandler($trans_data);
        // if($trans_data->getStatus() == ResponseHandler::STATUS_OK)
        // {
        //     $settingsData = $trans_data->getData();
        //     Calypso::getInstance()->session('settings', $settingsData);
        //     $this->set('settings', $settingsData);
        // }
    }

    public function merchant_transactions($id)
    {
        //Todo Load this asynchronously later to reduce response time
        $data = Calypso::getInstance()->session('user');

        if(empty($id))
        {
            Calypso::getInstance()->setFlashErrorMsg('Invalid Merchant selected.');
        }else{
            $merchantsAdp = new MerchantAdapter($data['id'], RequestHelper::getAccessToken());
            $response = $merchantsAdp->get($id);
            $rHandler = new ResponseHandler($response);


            if($rHandler->getStatus() == ResponseHandler::STATUS_OK){
                $this->set('merchant', $rHandler->getData());
            }
        }
    }

    public function merchant_sync($id)
    {
        //Todo Load this asynchronously later to reduce response time
        $data = Calypso::getInstance()->session('user');

        if(empty($id))
        {
            Calypso::getInstance()->setFlashErrorMsg('Invalid Merchant selected.');
        }else{
            $merchantsAdp = new MerchantAdapter($data['id'], RequestHelper::getAccessToken());
            $response = $merchantsAdp->get($id);
            $rHandler = new ResponseHandler($response);


            if($rHandler->getStatus() == ResponseHandler::STATUS_OK){
                $this->set('merchant', $rHandler->getData());
            }
        }
    }

    public function merchants()
    {

        $data = Calypso::getInstance()->session('user');

        $postData = Calypso::getInstance()->post(true);
        if(!empty($postData))
        {
            $check = $this->validateData($postData,['name','address','merchantbusinesstype',
            'bank','account_name','account_number','charge_type','charge','telephone','email','password']);
            if($check)
            {
                $newmerchant = new MerchantAdapter($data['id'], RequestHelper::getAccessToken());
                $response = $newmerchant->addMerchant(
                    $postData['name'],
                    $postData['address'],
                    $postData['telephone'],
                    $postData['merchantbusinesstype'],
                    $postData['charge_type'],
                    $postData['charge'],
                    $postData['account_number'],
                    $postData['bank'],
                    $postData['account_name'],
                    $data['id'],
                    $postData['email'],
                    $postData['password']
                );
                $rHandler = new ResponseHandler($response);
                if($rHandler->getStatus() == ResponseHandler::STATUS_OK){
                    Calypso::getInstance()->setFlashSuccessMsg('Merchant created successfully!');
                }else{
                    Calypso::getInstance()->setFlashErrorMsg($rHandler->getError());
                }
                Calypso::getInstance()->unsetSession('merchants'); // Clear cached list
            }
        }
        $cachedData = Calypso::getInstance()->session('merchants');
        if(empty($cachedData)){
            $merchantsAdp = new MerchantAdapter($data['id'], RequestHelper::getAccessToken());
            $trans_data = $merchantsAdp->getAll();
            $trans_data = new ResponseHandler($trans_data);
            if($trans_data->getStatus() == ResponseHandler::STATUS_OK)
            {
                $merchantstData = $trans_data->getData();
                Calypso::getInstance()->session('merchants', $merchantstData);
                $this->set('merchants', $merchantstData);
            }
        }else{
            $this->set('merchants', $cachedData);
        }

    }

    public function merchantDetail($id)
    {
        //Todo Load this asynchronously later to reduce response time
        $data = Calypso::getInstance()->session('user');

        if(empty($id))
        {
            Calypso::getInstance()->setFlashErrorMsg('Invalid Merchant selected.');
        }else{
            $postData = Calypso::getInstance()->post(true);
            if(!empty($postData) && !empty($postData['action_type']))
            {
                $action = $postData['action_type'];
                switch($action)
                {
                    case 'add_device_to_merchant':
                        if($this->validateData($postData,['device_code','address'],false)){
                            $merchantAdapter = new DeviceAdapter($data['id'], RequestHelper::getAccessToken());
                            $device_link_response = $merchantAdapter->assignDevice($postData['device_code'], $id);
                            $device_link_response = new ResponseHandler($device_link_response);
                            if($device_link_response->getStatus() == ResponseHandler::STATUS_OK)
                            {
                                Calypso::getInstance()->setFlashSuccessMsg('Device Added Successfully');
                            }else{
                                Calypso::getInstance()->setFlashErrorMsg($device_link_response->getError());
                            }
                        }else{
                            Calypso::getInstance()->setFlashSuccessMsg('Empty fields not allowed');
                        }
                        break;
                    case 'add_cashier':
                        if($this->validateData($postData,['email','telephone','password','firstname','lastname','gender'],false)){
                            $merchantAdapter = new MerchantAdapter($data['id'], RequestHelper::getAccessToken());
                            $newCashier = $merchantAdapter->addCashier($id,$postData['email'],$postData['password'],$postData['telephone'],$postData['firstname'],$postData['lastname'],$postData['gender']);
                            $newCashier = new ResponseHandler($newCashier);
                            if($newCashier->getStatus() == ResponseHandler::STATUS_OK)
                            {
                                Calypso::getInstance()->setFlashSuccessMsg('Cashier Added Successfully');
                            }else{
                                Calypso::getInstance()->setFlashErrorMsg($newCashier->getError());
                            }
                        }else{
                            Calypso::getInstance()->setFlashSuccessMsg('Empty fields not allowed');
                        }

                        break;
                }
            }


            $merchantsAdp = new MerchantAdapter($data['id'], RequestHelper::getAccessToken());
            $response = $merchantsAdp->get($id);
            $rHandler = new ResponseHandler($response);

            $cashiers = new CashierAdapter($data['id'], RequestHelper::getAccessToken());
            $cashier_response = $cashiers->getAllCashier($id);
            $cHandler = new ResponseHandler($cashier_response);

            $adapter2 = new MerchantAdapter($data['id'], RequestHelper::getAccessToken());
            $devices = $adapter2->getMerchantDevices($id);
            $dHandler = new ResponseHandler($devices);

            $adapter3 = new MerchantAdapter($data['id'], RequestHelper::getAccessToken());
            $alldevices = $adapter3->getAllDevices(0, 10000);
            $all_dHandler = new ResponseHandler($alldevices);


            if($rHandler->getStatus() == ResponseHandler::STATUS_OK){
                $this->set('merchant', $rHandler->getData());
            }
            if($cHandler->getStatus() == ResponseHandler::STATUS_OK){
                $this->set('cashiers', $cHandler->getData());
            }
            if($dHandler->getStatus() == ResponseHandler::STATUS_OK){
                $this->set('devices', $dHandler->getData());
            }
            if($all_dHandler->getStatus() == ResponseHandler::STATUS_OK){
                $this->set('all_devices', $all_dHandler->getData());
            }
        }
    }

    public function devices($id = null)
    {
        if(!is_null($id))
        {
            $this->renderCustom();
        }else{
            $data = Calypso::getInstance()->session('user');
            $deviceAdp = new DeviceAdapter($data['id'], RequestHelper::getAccessToken());
            $device_data = $deviceAdp->getAll(0,10000);
            $device_data = new ResponseHandler($device_data);
            if($device_data->getStatus() == ResponseHandler::STATUS_OK)
            {
                $deviceData = $device_data->getData();
                Calypso::getInstance()->session('devices', $deviceData['data']);
                $this->set('devices', $deviceData['data']);
            }
        }

    }

    public function cards($id=null)
    {
        if(!is_null($id))
        {
            $this->renderCustom();
        }else{
            $data = Calypso::getInstance()->session('user');
            $cardAdp = new CardAdapter($data['id'], RequestHelper::getAccessToken());
            $card_data = $cardAdp->getAll(0,10000);
            $card_data = new ResponseHandler($card_data);
            if($card_data->getStatus() == ResponseHandler::STATUS_OK)
            {
                $cardData = $card_data->getData();
                Calypso::getInstance()->session('cards', $cardData['data']);
                $this->set('cards', $cardData['data']);
            }
        }
    }

    public function transactions($id,$type)
    {
        if(empty($id) || empty($type))
        {
            Calypso::getInstance()->setFlashErrorMsg('Invalid Request. Please select the item again. If this persist, contact the admin');
        }else {
            $data = Calypso::getInstance()->session('user');
            $transactionAdp = new TransactionAdapter($data['id'], RequestHelper::getAccessToken());
            $transaction_data = $transactionAdp->getTransactions($id, $type,0, 100000); //For now
            $transaction_data = new ResponseHandler($transaction_data);
            if ($transaction_data->getStatus() == ResponseHandler::STATUS_OK) {
                $transactionData = $transaction_data->getData();
                switch($type) {
                    case 2:
                        //Merchant Transactions
                        $this->set('template', '');
                        break;
                    case 1:
                        //Agent's transactions
                        $this->set('template', '');
                        break;
                    case 3:
                        //Card transaction
                        $this->set('template', '_cardtransaction');
                        break;
                    case 4:
                        //Card statistics
                        $this->set('template', '');
                        break;
                    case 5:
                        //Card Transaction  by serial number
                        $this->set('template', '_cardtransaction');
                        break;
                }
                $this->set('transactions', $transactionData);
            }
        }
    }

    public function users()
    {
        $postData = Calypso::getInstance()->post(true);
        $data = Calypso::getInstance()->session('user');
        if(!empty($postData))
        {
            if($this->validateData($postData,['telephone','firstname','bp_serial','lastname','address']))
            {
                //Create a customer
                $createUserAdp = new UserAdapter($data['id'], RequestHelper::getAccessToken());
                $response = $createUserAdp->createCustomer(
                    $postData['bp_serial'],$postData['email'],$postData['telephone'], $postData['firstname'],$postData['lastname'],$postData['middlename'],$postData['gender'],$postData['address'],5,1
                );
                $handler = new ResponseHandler($response);
                if($handler->getStatus() == ResponseHandler::STATUS_OK){

                    Calypso::getInstance()->setFlashSuccessMsg('Customer registered successfully');
                }else{
                    Calypso::getInstance()->setFlashErrorMsg($handler->getError());
                }
            }else{
                Calypso::getInstance()->setFlashErrorMsg('Please ensure you fill telephone, first name, last name,address, and the BeepPay serial no fields');
            }

        }
        //Change this API to issuerusers/getall
        $userAdp = new UserAdapter($data['id'], RequestHelper::getAccessToken());
        $users_data = $userAdp->getAllByType(5,0,10000);
        $users_data = new ResponseHandler($users_data);
        if($users_data->getStatus() == ResponseHandler::STATUS_OK)
        {
            $usersData = $users_data->getData();
            $this->set('customers', $usersData);
        }
    }

    public function customerDetail($id)
    {
        if(!empty($id))
        {
            $data = Calypso::getInstance()->session('user');
            $userAdp = new UserAdapter($data['id'], RequestHelper::getAccessToken());
            $users_data = $userAdp->get($id);
            $resp = new ResponseHandler($users_data);
            if($resp->getStatus() == ResponseHandler::STATUS_OK){
                $this->set('data', $resp->getData());
            }
        }
        $this->doNotRenderHeader = 1;

        $this->renderCustom('_ajaxviews/customerdetail', true);
    }

    public function agentCollectionHistory($id)
    {
        if(empty($id))
        {
            Calypso::getInstance()->setFlashSuccessMsg('Collection history was not found for this agent');
        }else {
            $data = Calypso::getInstance()->session('user');


            $agentUsers = new UserAdapter($data['id'], RequestHelper::getAccessToken());
            $response = $agentUsers->get($id);
            $response = new ResponseHandler($response);

            $adapter2 = new DeviceAdapter($data['id'], RequestHelper::getAccessToken());
            $devices = $adapter2->getDevice($id);
            $devices = new ResponseHandler($devices);


            $transactions = new TransactionAdapter($data['id'], RequestHelper::getAccessToken());
            $responseTrans = $transactions->getTransactions($id,1,0,1000000);
            $responseTrans = new ResponseHandler($responseTrans);
            $transactionData = $responseTrans->getData();

            $this->set('collections', $transactionData);
            $this->set('devices', $devices->getData());
            $this->set('profile', $response->getData());


        }
    }

    public function customerContributionHistory($id)
    {
        if(empty($id))
        {
            Calypso::getInstance()->setFlashErrorMsg('Invalid customer specified');
        }else {
            $from = Calypso::getInstance()->get(true);
            if(!empty($from))
            {

            }
            $data = Calypso::getInstance()->session('user');

            $transactions = new TransactionAdapter($data['id'], RequestHelper::getAccessToken());
            $response2 = $transactions->getTransactions($id,6,0,100000);
            $response2 = new ResponseHandler($response2);
            $transactionData = $response2->getData();

            $this->set('collections', $transactionData);

        }
    }

    public function agentCreditHistory($agent_id)
    {
        if(empty($agent_id))
        {
            Calypso::getInstance()->setFlashErrorMsg('Invalid agent selected.');
        }else {
            $data = Calypso::getInstance()->session('user');
            $transactions = new TopupTransactionAdapter($data['id'], RequestHelper::getAccessToken());
            $response2 = $transactions->getHistory($agent_id,0,100000);

            $response2 = new ResponseHandler($response2);
            $transactionData = $response2->getData();
            $this->set('collections', $transactionData);
        }
    }

    public function updateNumber()
    {
        $this->doNotRenderHeader = 1;
        $postData = Calypso::getInstance()->post(true);
        $this->validateData($postData,['sanwo_id','sanwo_name','saving_card_no']);
        $data = Calypso::getInstance()->session('user');
        $customer = new Customer();
        $customer->where('sanwo_id',$postData['sanwo_id']);
        $checkCustomer = $customer->search();
        if(!empty($checkCustomer))
        {
            unset($customer);
            $customer = new Customer();
            $customer->id =  ($checkCustomer[0]['Customer']['id']);
        }
        $customer->issuer_id = $data['issuer']['id'];
        $customer->sanwo_id = $postData['sanwo_id'];
        $customer->fullname = $postData['sanwo_name'];
        $customer->saving_card_no = $postData['saving_card_no'];
        $id = $customer->save();
        Calypso::getInstance()->sendSuccessJSON(['status'=>'success','id'=>$id],"Customer's information updated successfully");

    }

    public function updateCycle()
    {
        $this->doNotRenderHeader = 1;
        $postData = Calypso::getInstance()->post(true);
        $this->validateData($postData,['cycle_value','cycle_field','customer_id']);
        $customerCycle = new CustomerCycle();
        $customerCycle->where('customer_id',$postData['customer_id']);
        $checkCustomer = $customerCycle->search();
        if(!empty($checkCustomer))
        {
            unset($customerCycle);
            $customerCycle = new CustomerCycle();
            $customerCycle->id =  ($checkCustomer[0]['CustomerCycle']['id']);
        }
        $customerCycle->cycle_id = $postData['cycle_field'];
        $customerCycle->customer_id = $postData['customer_id'];
        $customerCycle->special_charge = 0.00;
        $id = $customerCycle->save();
        Calypso::getInstance()->sendSuccessJSON(['status'=>'success','id'=>$id],"Customer's information updated successfully");

    }
}