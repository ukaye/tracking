<?php

use SanwoPHPAdapter\UserAdapter;
use SanwoPHPAdapter\Util\Response;
use SanwoPHPAdapter\ResponseHandler;
use SanwoPHPAdapter\TransactionAdapter;
use SanwoPHPAdapter\AgentAdapter;
use SanwoPHPAdapter\RequestHelper;
use SanwoPHPAdapter\DeviceAdapter;
use SanwoPHPAdapter\CardAdapter;
use SanwoPHPAdapter\CustomerAdapter;
use SanwoPHPAdapter\IssuerAdapter;
use SanwoPHPAdapter\MerchantAdapter;
use SanwoPHPAdapter\CashierAdapter;
use SanwoPHPAdapter\TopupTransactionAdapter;
use SanwoPHPAdapter\SettingsAdapter;
use SanwoPHPAdapter\Globals\ServiceConstant;


class AgentController extends VanillaController
{
    private $noAuth = [];
    public function beforeAction() {
        if(in_array($this->_action, $this->noAuth)) {
            return true;
        }
        parent::beforeAction();
    }

    public function index(){
    	$data = Calypso::getInstance()->session('user');

        //create agent comes here
         $postData = Calypso::getInstance()->post(true);

         
         if (!empty($postData)){
            $postData['user_type_id'] = 4;
            $postData['status'] = 1;
            var_dump($postData);

            $check =$this->validateData($postData, ['firstname', 'lastname', 'email', 'telephone',
             'gender', 'user_type_id', 'status']);

            if($check){
                $agent = new UserAdapter($data['id'], RequestHelper::getAccessToken());
                $request = $agent->createAgent(
                        $postData['email'],
                        $postData['telephone'],
                        $postData['password'],
                        $postData['user_type_id'],
                        $postData['status']       
                    );

                $response = new ResponseHandler($request);
                if($response->getStatus() == ResponseHandler::STATUS_OK){
                    Calypso::getInstance()->setFlashSuccessMsg('Agent created successfully!');
                }else{

                    Calypso::getInstance()->setFlashErrorMsg($response->getError());
                }
             //   Calypso::getInstance()->unsetSession('merchants'); // Clear cached list
                //var_dump($response);

            }


         }

        $issuerAdapter = new IssuerAdapter($data['id'], RequestHelper::getAccessToken());

        $trans_data = $issuerAdapter->getIssuerUsers(16, 4);


        //var_dump($trans_data);


        
        $trans_data = new ResponseHandler($trans_data);

        //var_dump($trans_data);
        if($trans_data->getStatus() == ResponseHandler::STATUS_OK)
        {
            $agentData = $trans_data->getData();
            Calypso::getInstance()->session('agents', $agentData);
            $this->set('agents', $agentData);
        }
    }
}

?>