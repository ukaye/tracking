<?php
/**
 * Created by Adeinka.
 * User: CodeBeast
 * Date: 10/26/2015
 * Time: 5:05 AM
 */


use SanwoPHPAdapter\UserAdapter;
use SanwoPHPAdapter\Util\Response;
use SanwoPHPAdapter\ResponseHandler;
use SanwoPHPAdapter\TransactionAdapter;
use SanwoPHPAdapter\AgentAdapter;
use SanwoPHPAdapter\RequestHelper;
use SanwoPHPAdapter\DeviceAdapter;
use SanwoPHPAdapter\CardAdapter;
use SanwoPHPAdapter\CustomerAdapter;
use SanwoPHPAdapter\IssuerAdapter;
use SanwoPHPAdapter\MerchantAdapter;
use SanwoPHPAdapter\CashierAdapter;
use SanwoPHPAdapter\TopupTransactionAdapter;
use SanwoPHPAdapter\SettingsAdapter;
use SanwoPHPAdapter\Globals\ServiceConstant;
class MerchantController extends VanillaController
{
    private $noAuth = [];
    public function beforeAction() {
        if(in_array($this->_action, $this->noAuth)) {
            return true;
        }
        parent::beforeAction();
    }

    public function index(){
    	
	   	$data = Calypso::getInstance()->session('user');

        $postData = Calypso::getInstance()->post(true);

        if(!empty($postData))
        {
            $check = $this->validateData($postData,['name','address','merchantbusinesstype',
            'bank','account_name','account_number','charge_type','charge','telephone','email','password']);
            if($check)
            {
                $newmerchant = new MerchantAdapter($data['id'], RequestHelper::getAccessToken());
                $response = $newmerchant->addMerchant(
                    $postData['name'],
                    $postData['address'],
                    $postData['telephone'],
                    $postData['merchantbusinesstype'],
                    $postData['charge_type'],
                    $postData['charge'],
                    $postData['account_number'],
                    $postData['bank'],
                    $postData['account_name'],
                    $data['id'],
                    $postData['email'],
                    $postData['password']
                );
                $rHandler = new ResponseHandler($response);
                if($rHandler->getStatus() == ResponseHandler::STATUS_OK){
                    Calypso::getInstance()->setFlashSuccessMsg('Merchant created successfully!');
                }else{
                    Calypso::getInstance()->setFlashErrorMsg($rHandler->getError());
                }
                Calypso::getInstance()->unsetSession('merchants'); // Clear cached list
            }
        }
        $cachedData = Calypso::getInstance()->session('merchants');
        if(empty($cachedData)){
            $merchantsAdp = new MerchantAdapter($data['id'], RequestHelper::getAccessToken());
            $trans_data = $merchantsAdp->getAll();
            $trans_data = new ResponseHandler($trans_data);
            if($trans_data->getStatus() == ResponseHandler::STATUS_OK)
            {
                $merchantstData = $trans_data->getData();
                Calypso::getInstance()->session('merchants', $merchantstData);
                $this->set('merchants', $merchantstData);
            }
        }else{
            $this->set('merchants', $cachedData);
        }
    }

    public function details($id){
    	$data = Calypso::getInstance()->session('user');

        if(empty($id))
        {
            Calypso::getInstance()->setFlashErrorMsg('Invalid Merchant selected.');
        }else{
            $postData = Calypso::getInstance()->post(true);
            if(!empty($postData) && !empty($postData['action_type']))
            {
                $action = $postData['action_type'];
                switch($action)
                {
                    case 'add_device_to_merchant':
                        if($this->validateData($postData,['device_code','address'],false)){
                            $merchantAdapter = new DeviceAdapter($data['id'], RequestHelper::getAccessToken());
                            $device_link_response = $merchantAdapter->assignDevice($postData['device_code'], $id);
                            $device_link_response = new ResponseHandler($device_link_response);
                            if($device_link_response->getStatus() == ResponseHandler::STATUS_OK)
                            {
                                Calypso::getInstance()->setFlashSuccessMsg('Device Added Successfully');
                            }else{
                                Calypso::getInstance()->setFlashErrorMsg($device_link_response->getError());
                            }
                        }else{
                            Calypso::getInstance()->setFlashSuccessMsg('Empty fields not allowed');
                        }
                        break;
                    case 'add_cashier':
                        if($this->validateData($postData,['email','telephone','password','firstname','lastname','gender'],false)){
                            $merchantAdapter = new MerchantAdapter($data['id'], RequestHelper::getAccessToken());
                            $newCashier = $merchantAdapter->addCashier($id,$postData['email'],$postData['password'],$postData['telephone'],$postData['firstname'],$postData['lastname'],$postData['gender']);
                            $newCashier = new ResponseHandler($newCashier);
                            if($newCashier->getStatus() == ResponseHandler::STATUS_OK)
                            {
                                Calypso::getInstance()->setFlashSuccessMsg('Cashier Added Successfully');

                                $postData = null;

                            }else{
                                Calypso::getInstance()->setFlashErrorMsg($newCashier->getError());
                            }
                        }else{
                            Calypso::getInstance()->setFlashSuccessMsg('Empty fields not allowed');
                        }

                        break;
                }
            }


            $merchantsAdp = new MerchantAdapter($data['id'], RequestHelper::getAccessToken());
            $response = $merchantsAdp->get($id);
            $rHandler = new ResponseHandler($response);

           // var_dump($rHandler);

            $cashiers = new CashierAdapter($data['id'], RequestHelper::getAccessToken());
            $cashier_response = $cashiers->getAllCashier($id);
            $cHandler = new ResponseHandler($cashier_response);
           // var_dump($cHandler);

            $adapter2 = new MerchantAdapter($data['id'], RequestHelper::getAccessToken());
            $devices = $adapter2->getMerchantDevices($id);
            $dHandler = new ResponseHandler($devices);
            //var_dump($dHandler);

            $adapter3 = new MerchantAdapter($data['id'], RequestHelper::getAccessToken());
            $alldevices = $adapter3->getAllDevices(0, 10000);
            $all_dHandler = new ResponseHandler($alldevices);

          //  var_dump($all_dHandler);


            if($rHandler->getStatus() == ResponseHandler::STATUS_OK){
                $this->set('merchant', $rHandler->getData());
               
            }
            if($cHandler->getStatus() == ResponseHandler::STATUS_OK){
                $this->set('cashiers', $cHandler->getData());
            }
            if($dHandler->getStatus() == ResponseHandler::STATUS_OK){
                $this->set('devices', $dHandler->getData());
            }
            if($all_dHandler->getStatus() == ResponseHandler::STATUS_OK){
                $this->set('all_devices', $all_dHandler->getData());
            }
        }
    }

    public function syncs($id){
    	$data = Calypso::getInstance()->session('user');

        if(empty($id))
        {
            Calypso::getInstance()->setFlashErrorMsg('Invalid Merchant selected.');
        }
        else{
        	$merchantsAdp = new MerchantAdapter($data['id'], RequestHelper::getAccessToken());
            $response = $merchantsAdp->get($id);
            $rHandler = new ResponseHandler($response);

            if($rHandler->getStatus() == ResponseHandler::STATUS_OK){
                $this->set('merchant', $rHandler->getData());
               
               $transactionAdapter = new TransactionAdapter($data['id'], RequestHelper::getAccessToken());

              $request = $transactionAdapter->getMerchantSyncSummary(0, 1000);
               $response = new ResponseHandler($request);
              //  var_dump($response->getData());

              if ($response->getStatus() == ResponseHandler::STATUS_OK){
              		$this->set('sync_transactions', $response->getData());
              }
              else{
              	 Calypso::getInstance()->setFlashErrorMsg($response->getError());
              }
             

             
            
        }
    }
}




    public function transactions($id, $check_history_id=null){
    	$data = Calypso::getInstance()->session('user');

    	echo 'check history id=' . $check_history_id;

    	 if(empty($id))
        {
            Calypso::getInstance()->setFlashErrorMsg('Invalid Merchant selected.');
        }

        else{

        	$merchantsAdp = new MerchantAdapter($data['id'], RequestHelper::getAccessToken());
            $response = $merchantsAdp->get($id);
            $rHandler = new ResponseHandler($response);

            if($rHandler->getStatus() == ResponseHandler::STATUS_OK){
                $this->set('merchant', $rHandler->getData());
               
               $transactionAdapter = new TransactionAdapter($data['id'], RequestHelper::getAccessToken());

               $transactionAdapter = $transactionAdapter->getMerchantTransaction($id, 1000, 0, 2);

               $result = new ResponseHandler($transactionAdapter);

             if ($result->getStatus()  == ResponseHandler::STATUS_OK){
             	$this->set('transactions', $result->getData());

             	//var_dump($result->getData());
             }
             else
             {
             	 Calypso::getInstance()->setFlashErrorMsg('Error loading transactions');
             }
               
            }
            else{
            	Calypso::getInstance()->setFlashErrorMsg("Unable to get merchant's details, try again later");
            }
            
        }

    }


}