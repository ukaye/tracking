<?php
class BasicEncrypt {
    public static function doOneWayEncrypt($data, $count = null){
        if($count==null) {
            return sha1(md5($data . HASH_KEY) . HASH_KEY);
        }
        return self::extractCode(sha1(md5($data . HASH_KEY) . HASH_KEY),$count);
    }

    private static function extractCode($token,$count=5){
        return substr($token,0,$count);
    }

    public static function keyGen($seed = null) {
        if(is_null($seed)) {
            $seed = sha1(time(). mt_rand(200, 10000000));
        }
        return base64_encode(self::extractCode($seed, 7));
    }
    public static function generatePassword() {
        $p = self::keyGen();
        $main_key = explode('==',$p);
        return $main_key[0];
    }
}