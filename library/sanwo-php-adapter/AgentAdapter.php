<?php


namespace SanwoPHPAdapter;
use SanwoPHPAdapter\Globals\ServiceConstant;

class AgentAdapter extends BaseAdapter {

    private function updateAgentCredit($agent_id,$type,$amount){
        return $this->request(ServiceConstant::URL_CREDIT_AGENT,array(
            'agent_id'=>$agent_id,
            'type' => $type,
            'amount' => $amount
        ),self::HTTP_POST);
    }

    
    public function debitAgent($agent_id,$amount){
        return $this->updateAgentCredit($agent_id,2,$amount);
    }
    public function creditAgent($agent_id,$amount){
        return $this->updateAgentCredit($agent_id,1,$amount);
    }
    public function getAgents(){
        return $this->request(
            ServiceConstant::URL_GET_AGENT,
            array(),
            self::HTTP_GET
        );
    }
} 