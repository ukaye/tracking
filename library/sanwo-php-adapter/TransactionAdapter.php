<?php


namespace SanwoPHPAdapter;


class TransactionAdapter extends BaseAdapter {

    public function getTransactions($id,$type,$offset,$count){
        switch($type){
            case 2:
                return $this->request(
                    "credittransaction/getAll",
                    array(
                        'merchant_id' => $id,
                        'device_type_id' => $type,
                        'count' => $count,
                        'offset' => $offset,
                        'with_card'=>1
                    ),
                    self::HTTP_GET
                );
                break;
            case 1:
                return $this->request(
                    "credittransaction/getAll",
                    array(
                        'agent_id' => $id,
                        'count' => $count,
                        'offset' => $offset,
                        'with_card'=>1
                    ),
                    self::HTTP_GET
                );
                break;
            case 3:
                return $this->request(
                    "credittransaction/getAll",
                    array(
                        'serial_number' => $id,
                        'count' => $count,
                        'device_type_id' => 2,
                        'with_merchant' => 1,
                        'offset' => $offset
                    ),
                    self::HTTP_GET
                );
                break;
            case 4:
                return $this->request(
                    "credittransaction/getcardstats",
                    array(
                        'count' => $count,
                        'offset' => $offset
                    ),
                    self::HTTP_GET
                );
                break;
            case 5:
                return $this->getCardTransactions($id, $count, $offset);
                break;
            case 6:
                return $this->request(
                    "credittransaction/getAll",
                    array(
                        'serial_number' => $id,
                        'count' => $count,
                        'device_type_id' => 1,
                        'with_agent' => 1,
                        'offset' => $offset
                    ),
                    self::HTTP_GET
                );
                break;
        }
    }
    public function getCardTransactions($serial_number,$offset,$count){
        //getTransactedMerchant
        return $this->request(
            "credittransaction/getTransactedMerchant",
            array(
                'serial_number' => $serial_number,
                'count' => $count,
                'offset' => $offset
            ),
            self::HTTP_GET
        );
    }


    public function getMerchantTransaction($id, $count, $offset, $type, $check_history_id=null){

        return $this->request(
                    "credittransaction/getAll",
                    array(
                        'merchant_id' => $id,
                        'device_type_id' => $type,
                        'count' => $count,
                        'offset' => $offset,
                        'with_card'=>1,
                        'check_history_id' =>$check_history_id,
                        'wtih_holder' =>1
                        
                    ),
                    self::HTTP_GET
                );
    }



    public function getMerchantSyncSummary($offset, $count, $merchant_id){
         return $this->request(
                    "checkhistory/fetchAll",
                    array(
                        'offset' => $offset,
                        'count' => $count,
                        
                        
                    ),
                    self::HTTP_GET
                );
    }
} 