<?php
/**
 * Created by PhpStorm.
 * User: epapa
 * Date: 5/2/15
 * Time: 1:59 PM
 */

namespace SanwoPHPAdapter;


class ReportAdapter extends BaseAdapter
{
    public function merchantSummary($offset = 0, $count = 20)
    {
        return $this->request(
            "report/merchantSummary",
            array(
                'count' => $count,
                'offset' => $offset
            ),
            self::HTTP_GET
        );
    }

    public function issuerMerchants($offset = 0, $count = 20)
    {
        return $this->request(
            "merchant/getall",
            array(
                'count' => $count,
                'offset' => $offset
            ),
            self::HTTP_GET
        );
    }
}