<?php


namespace SanwoPHPAdapter;


class TopupTransactionAdapter extends BaseAdapter
{

    /**
     * @author Adeyemi Olaoye <adeyemi@sanwo.me>
     * @param $email
     * @param $agent_id
     * @param $amount
     * @return array|mixed|string
     */
    public function addTopup($email, $agent_id, $amount)
    {
        return $this->request(
            "topup/add/",
            array(
                'email' => $email,
                'agent_id' => $agent_id,
                'amount' => $amount
            ),
            self::HTTP_POST
        );
    }

    public function getHistory($agent_id, $offset=0, $count = 20)
    {
        return $this->request(
            "topuptransaction/getall/",
            array(
                'offset' => $offset,
                'agent_id' => $agent_id,
                'count' => $count,
                'with_agent' => 1,
                'with_device' => 1
            ),
            self::HTTP_GET
        );
    }

} 