<?php
/**
 * Created by PhpStorm.
 * User: epapa
 * Date: 5/2/15
 * Time: 8:20 AM
 */
namespace SanwoPHPAdapter;

class RequestHelper {

    /**
     * @return mixed
     */
    public static function getAccessToken()
    {
        return \Calypso::getInstance()->session('access_token');
    }

    /**
     * @param mixed $accessToken
     */
    public static function setAccessToken($accessToken)
    {
        \Calypso::getInstance()->session("access_token", $accessToken);
    }

}