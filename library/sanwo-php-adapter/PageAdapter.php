<?php

namespace SanwoPHPAdapter;

use SanwoPHPAdapter\Globals\ServiceConstant;

class PageAdapter extends BaseAdapter{

    public function getPageStatistics($filters){
        $str = join('=1&',$filters);
        $str.= '=1';
        return $this->request(ServiceConstant::URL_GET_PAGE_SUMMARY.'?'.$str,array(), self::HTTP_GET);
    }
}