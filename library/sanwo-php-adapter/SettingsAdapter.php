<?php


namespace SanwoPHPAdapter;


use SanwoPHPAdapter\Globals\ServiceConstant;

class SettingsAdapter extends BaseAdapter
{

    /**
     * @author Ita Ukemeabasi
     * @param $name
     * @param $address
     * @param $phone_number
     * @param $business_type
     * @param $charge_type
     * @param $charge
     * @param $account_number
     * @param $bank
     * @param $account_name
     * @param $created_by
     * @param $email
     * @param $password
     * @return array|mixed|string
     */
    public function SettingsAdapter($name, $address, $phone_number, $business_type, $charge_type, $charge, $account_number, $bank, $account_name, $created_by, $email, $password)
    {
        return $this->request(
            "settings/addSettings/",
            array(
                'name' => $name,
                'address' => $address,
                'phone_number' => $phone_number,
                'business_type' => $business_type,
                'charge_type' => $charge_type,
                'charge' => $charge,
                'account_number' => $account_number,
                'bank' => $bank,
                'account_name' => $account_name,
                'created_by' => $created_by,
                'email' => $email,
                'password' => $password,
            ),
            self::HTTP_POST
        );
    }

    /**
     * @author Ita Ukemeabasi
     * @param $id
     * @param $status
     * @return array|mixed|string
     */
    public function changeStatus($id, $status)
    {
        return $this->request(
            "settings/changeStatus/",
            array(
                'id' => $id,
                'status' => $status,
            ),
            self::HTTP_POST
        );
    }

    /**
     * @author Ita Ukemeabasi
     * @param $id
     * @return array|mixed|string
     */
    public function remove($id)
    {
        return $this->request(
            "settings/remove/",
            array(
                'id' => $id
            ),
            self::HTTP_POST
        );
    }

    /**
     * @author Ita Ukemeabasi
     * @param $id
     * @param $name
     * @param $address
     * @param $phone_number
     * @param $business_type
     * @param $charge_type
     * @param $charge
     * @param $account_number
     * @param $bank
     * @param $account_name
     * @return array|mixed|string
     */
    public function edit($id, $name, $address, $phone_number, $business_type, $charge_type, $charge, $account_number, $bank, $account_name)
    {
        return $this->request(
            "settings/edit/",
            array(
                'id' => $id,
                'name' => $name,
                'address' => $address,
                'phone_number' => $phone_number,
                'business_type' => $business_type,
                'charge_type' => $charge_type,
                'charge' => $charge,
                'account_number' => $account_number,
                'bank' => $bank,
                'account_name' => $account_name,
            ),
            self::HTTP_POST
        );
    }

    /**
     * @author Ita Ukemeabasi
     * @param $id
     * @return array|mixed|string
     */
    public function get($id)
    {
        return $this->request(
            "settings/get/",
            array(
                'id' => $id
            ),
            self::HTTP_GET
        );
    }
} 