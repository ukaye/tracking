<?php

class HTML {
	private $js = array();

	function shortenUrls($data) {
		$data = preg_replace_callback('@(https?://([-\w\.]+)+(:\d+)?(/([\w/_\.]*(\?\S+)?)?)?)@', array(get_class($this), '_fetchTinyUrl'), $data);
		return $data;
	}

	private function _fetchTinyUrl($url) { 
		$ch = curl_init(); 
		$timeout = 5; 
		curl_setopt($ch,CURLOPT_URL,'http://tinyurl.com/api-create.php?url='.$url[0]); 
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1); 
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout); 
		$data = curl_exec($ch); 
		curl_close($ch); 
		return '<a href="'.$data.'" target = "_blank" >'.$data.'</a>'; 
	}

	function sanitize($data) {
		return mysql_real_escape_string($data);
	}

	function link($text,$path,$new_win=false,$prompt = null,$confirmMessage = "Are you sure?") {
		$path = str_replace(' ','-',$path);
		if ($prompt) {
			$data = '<a href="javascript:void(0);" onclick="javascript:jumpTo(\''.BASE_PATH.'/'.$path.'\',\''.$confirmMessage.'\')">'.$text.'</a>';
		} else {
			$data = '<a '.($new_win?'target="_blank"':'').' href="'.BASE_PATH.'/'.$path.'">'.$text.'</a>';
		}
		return $data;
	}

	function includeJs($fileName) {
		$data = '<script src="'.BASE_PATH.'/js/'.$fileName.'.js"></script>';
		return $data;
	}

	function includeCss($fileName) {
		$data = '<style href="'.BASE_PATH.'/css/'.$fileName.'.css"></script>';
		return $data;
	}
    static function makeSelectField($id,$name,$descriptor=null,$data,$key,$value,$class=array(),$default_key=null,$multiple=false,$others = array()){
        $str = "<select ".($multiple ?'multiple':'')." id='".$id."' name='".$name."' class='";
        if(!is_array($class)){
            $str.= $class."'";
        }else{
            foreach($class as $c){
                $str.= $c." ";
            }
            $str.= "'";
        }
        $str.= ">";
        if(is_array($data) && !empty($data)){
            $c = 0;
            if(!$multiple) {
                $str .= "<option>Select...</option>";
            }else{
                $str .= "<option></option>";
            }

            foreach($data as $item){
                if($descriptor != null && is_array($descriptor)){
                    $selected = $default_key===$item[$descriptor[0]][$key] ? 'selected':'';
                    $str.= "<option ".$selected." value='".$item[$descriptor[0]][$key]."'>".$item[$descriptor[0]][$value]."</option>";
                }else{
                    if(!is_array($item)){
                        $selected = $default_key===$c ? 'selected':'';
                        $str.= "<option ".$selected." value='".$c++."'>".$item."</option>";
                    }else {
                        $selected = $default_key===$item[$key] ? 'selected':'';
                        $str .= "<option ".$selected." value='" . $item[$key] . "'>" . $item[$value] . "</option>";
                    }
                }
            }
        }
        if(!empty($others)){
            foreach($others as $k => $v){
                $str .= "<option value='".$k."'>".$v."</option>";
            }
        }
        $str.= "</select>";
        return $str;
    }
}