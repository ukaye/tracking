var app = angular.module('myApp', ['ngRoute']);

app.config(['$routeProvider',
  function ($routeProvider) {
        $routeProvider.
        when('/home', {
            title: 'Home',
            templateUrl: 'partials/home.html',
        })
            .when('/agents', {
                title: 'Agents',
                templateUrl: 'partials/agents.html',
            })
            .otherwise({
                redirectTo: '/home'
            });
  }])