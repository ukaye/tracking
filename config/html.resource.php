<?php
$allowedJS = array(
    'jquery' => '/js/jquery.js',
    'bootstrap' => '/js/bootstrap.min.js',
    'generic' => '/js/generic.js',
    'jquery.multiSelect' => '/js/jquery.multiSelect.js',
    'cedarjs' => '/js/cedarjs.js'
);

$allowedCSS = array(
    'bootstrap'=>'/css/bootstrap.min.css',
    'style'=>'/css/style.css',
    'jquery.multiSelect'=>'/css/jquery.multiSelect.css'
);

