/*
SQLyog Community v12.09 (32 bit)
MySQL - 5.5.43-0+deb7u1 : Database - gobcdb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`gobcdb` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `gobcdb`;

/*Table structure for table `backoffices` */

DROP TABLE IF EXISTS `backoffices`;

CREATE TABLE `backoffices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `backoffices` */

insert  into `backoffices`(`id`,`email`,`password`,`date_time`,`status`) values (1,'gobc@registration.com','9ea54f9de5f9979961548a39728b157b5a761198','2015-06-02 04:22:52',1);

/*Table structure for table `churches` */

DROP TABLE IF EXISTS `churches`;

CREATE TABLE `churches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `churches` */

insert  into `churches`(`id`,`name`,`status`) values (1,'LOIC Headquarters',1),(2,'LOIC Ife',1),(3,'LOIC Ilesha',1),(4,'LOIC Ibadan',1),(5,'LOIC Ikirun',1),(6,'LOIC Akure',1),(7,'LOIC London',1),(8,'LOIC USA',1),(9,'Other Churches',1),(10,'Alagbaka parish',1),(11,'Other Church on the street',1),(12,'Other Church on the street',1),(13,'Test',1);

/*Table structure for table `countries` */

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `countries` */

insert  into `countries`(`id`,`name`,`status`) values (1,'Nigeria',1);

/*Table structure for table `coupons` */

DROP TABLE IF EXISTS `coupons`;

CREATE TABLE `coupons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `value` double DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `coupons` */

insert  into `coupons`(`id`,`name`,`value`,`status`) values (1,'gobc2015',2000,1);

/*Table structure for table `posts` */

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `posts` */

insert  into `posts`(`id`,`name`,`status`) values (1,'General Overseer',1),(2,'Founder',1),(3,'Pastor',1),(4,'Evangelist',1),(5,'Prophet',1),(6,'Member',1),(7,'Teacher',1),(8,'Deacon',1),(9,'Deaconess',1),(10,'Others',1),(11,'Monitor',1),(12,'Monitor',1),(13,'Test Post',1),(14,'Director',1);

/*Table structure for table `registrations` */

DROP TABLE IF EXISTS `registrations`;

CREATE TABLE `registrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(256) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `sex` varchar(2) DEFAULT NULL,
  `church_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `town` varchar(128) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `first_timer` tinyint(4) DEFAULT NULL,
  `reprint_token` varchar(7) DEFAULT NULL,
  `early_bird` tinyint(4) DEFAULT NULL,
  `is_coupon` tinyint(4) DEFAULT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `pre_order_message` tinyint(4) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `message_amount` double DEFAULT NULL,
  `payment_status` tinyint(4) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `registrations` */

insert  into `registrations`(`id`,`fullname`,`email`,`phone`,`sex`,`church_id`,`post_id`,`town`,`state_id`,`first_timer`,`reprint_token`,`early_bird`,`is_coupon`,`coupon_id`,`pre_order_message`,`amount`,`message_amount`,`payment_status`,`date_time`,`status`) values (1,'Adewole Toyin adewale 74','admin@fox.com','08099536787','M',2,11,'rfse srtdfbsbd er ser sss fse f dvzsdvsv',0,2,'07E2C6D',2,1,1,1,2000,1000,1,'2015-06-02 02:03:00',1),(2,'Adewole Toyin adewale 74','admin@fox.com','08099536787','M',2,11,'rfse srtdfbsbd er ser sss fse f dvzsdvsv',0,2,'E4465C9',2,1,1,1,2000,1000,1,'2015-06-02 03:16:00',1),(3,'Adewole Toyin adewale 74','admin@fox.com','08099536787','M',2,11,'rfse srtdfbsbd er ser sss fse f dvzsdvsv',0,2,'2296040',2,1,1,1,2000,1000,1,'2015-06-02 03:43:00',1),(4,'Adewole Toyin adewale 74','admin@fox.com','08099536787','M',2,11,'rfse srtdfbsbd er ser sss fse f dvzsdvsv',0,2,'688D578',2,1,1,1,2000,1000,1,'2015-06-02 03:21:00',1),(5,'Adewole Toyin adewale Kuti','kuti@test.com','08077667755','M',1,9,'Ile-ife ',0,1,'08D5BBA',2,1,1,1,2000,1000,1,'2015-06-02 11:33:00',1),(6,'Adewole Toyin adewale 7434','wapicadmin@wapic.com','08099536787','M',12,6,'rfse srtdfbsbd er ser sss fse f dvzsdvsv',3,1,'45C8CE1',2,2,-1,1,3000,1000,1,'2015-06-03 10:58:00',1),(7,NULL,NULL,NULL,NULL,0,0,NULL,0,2,'F68D0ED',2,2,-1,2,3000,1000,1,'2015-06-03 10:53:00',1),(8,NULL,NULL,NULL,NULL,0,0,NULL,0,2,'EF96FA8',2,2,-1,2,3000,1000,1,'2015-06-03 10:22:00',1),(9,NULL,NULL,NULL,NULL,0,0,NULL,0,2,'2E1FFB7',2,2,-1,2,3000,1000,1,'2015-06-03 10:55:00',1),(10,'Adewole Toyin adewale 7434','wapicadmin@wapic.com','08099536787','M',12,6,'rfse srtdfbsbd er ser sss fse f dvzsdvsv',3,1,'D632D55',2,2,-1,1,3000,1000,1,'2015-06-03 11:23:00',1),(11,'Adekunle Ojo','wapicadmin@wapic.com','08076543455','M',8,4,'rfse srtdfbsbd er ser sss fse f dvzsdvsv',3,1,'7E7F1A9',2,2,-1,1,3000,1000,1,'2015-06-03 12:33:00',1);

/*Table structure for table `states` */

DROP TABLE IF EXISTS `states`;

CREATE TABLE `states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `states` */

insert  into `states`(`id`,`country_id`,`name`,`status`) values (1,1,'ABIA',1),(2,1,'ADAMAWA ',1),(3,1,'AKWA IBOM',1),(4,1,'ANAMBRA',1),(5,1,'BAUCHI',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
