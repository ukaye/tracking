/*
SQLyog Community v12.09 (64 bit)
MySQL - 5.6.17 : Database - fedpolydb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`fedpolydb` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `fedpolydb`;

/*Table structure for table `departments` */

DROP TABLE IF EXISTS `departments`;

CREATE TABLE `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_id` int(11) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Data for the table `departments` */

insert  into `departments`(`id`,`school_id`,`name`,`status`) values (1,1,'Basic Sciences',1),(2,1,'Computer Science',1),(3,1,'Hospitality Leisure & Tourism Management\r\n',1),(4,1,'Geological Technology',1),(5,1,'Nutrition and Dietetics  ',1),(6,1,'Science Laboratory Technology',1),(7,1,'Statistics',1),(8,2,'Accountancy',1),(9,2,'Banking and Finance ',1),(10,2,'Business Administration and Management\r\n',1),(11,2,'Library and Information Science ',1),(12,2,'Marketing',1),(13,2,'Office Technology and Management\r\n',1),(14,3,'Computer Engineering',1),(15,3,'Civil Engineering',1),(16,3,'Electrical Electronics   Engineering',1),(17,3,'Mechanical Engineering',1),(18,4,'Architectural Technology\r\n',1),(19,4,'Building Technology\r\n',1),(20,4,'Estate Management',1),(21,4,'Survey and Geoinformatics',1);

/*Table structure for table `exams` */

DROP TABLE IF EXISTS `exams`;

CREATE TABLE `exams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_index` tinyint(4) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `exams` */

insert  into `exams`(`id`,`item_index`,`name`,`status`) values (1,1,'WAEC',1),(2,2,'NECO',1);

/*Table structure for table `grades` */

DROP TABLE IF EXISTS `grades`;

CREATE TABLE `grades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `code` varchar(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `grades` */

insert  into `grades`(`id`,`name`,`code`,`status`) values (1,'A1',NULL,1),(2,'B2',NULL,1),(3,'B3',NULL,1),(4,'C4',NULL,1),(5,'C5',NULL,1),(6,'C6',NULL,1),(7,'D7',NULL,1),(8,'E8',NULL,1),(10,'F9',NULL,1);

/*Table structure for table `guardians` */

DROP TABLE IF EXISTS `guardians`;

CREATE TABLE `guardians` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `address` text,
  `relationship_id` int(11) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `guardians` */

insert  into `guardians`(`id`,`student_id`,`name`,`address`,`relationship_id`,`telephone`,`date_time`,`status`) values (6,9,'Akinboyewa Akindolani','Software Engineering Lab,Centre of Excellence in software engineering, Obafemi Awolowo University, Ile-Ife, Osun State. Nigeria',1,'08032280098','2015-06-15 05:37:00',1);

/*Table structure for table `localgovs` */

DROP TABLE IF EXISTS `localgovs`;

CREATE TABLE `localgovs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `localgovs` */

/*Table structure for table `nationalities` */

DROP TABLE IF EXISTS `nationalities`;

CREATE TABLE `nationalities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `nationalities` */

insert  into `nationalities`(`id`,`name`,`status`) values (1,'Nigerian',1),(2,'Others',1);

/*Table structure for table `programmes` */

DROP TABLE IF EXISTS `programmes`;

CREATE TABLE `programmes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

/*Data for the table `programmes` */

insert  into `programmes`(`id`,`department_id`,`name`,`status`) values (1,1,'Pre - ND Science',1),(2,2,'ND Computer Science',1),(3,2,'HND Computer Science',1),(4,2,'ND PT Computer Science',1),(5,3,'ND Hospitality Management',1),(6,3,'HND Hospitality Management',1),(7,3,'ND Leisure and Tourism Management',1),(8,3,'HND Leisure and Tourism Management',1),(9,3,'ND PT Hospitality Management',1),(10,4,'ND Geological Technology',1),(11,5,'ND nutrition and Dietetics',1),(12,6,'ND Science Laboratory Technology',1),(13,6,'ND PT Science Laboratory Technology',1),(14,6,'HND S.L.T. (Biochemistry)',1),(15,6,'HND S.L.T. (Chemistry)',1),(16,6,'HND S.L.T. (Microbiology)',1),(17,6,'HND S.L.T. (Physics with Electronics)',1),(18,7,'ND Statistics',1),(19,7,'ND PT Statistic',1),(20,7,'HND Statistics',1),(21,8,'ND Accountancy',1),(22,8,'ND PT Accountancy',1),(23,8,'HND Accountancy',1),(24,9,'ND Banking and Finance ',1),(25,9,'ND PT Banking and Finance ',1),(26,9,'HND Banking and Finance ',1),(27,10,'ND Business Administration and Management',1),(28,10,'ND PT Business Administration and Management',1),(29,10,'HND Business Administration and Management',1),(30,11,'ND Library and Information Studies    ',1),(31,12,'HND Marketing',1),(32,13,'ND Office Technology and Management',1),(33,13,'ND PT Office Technology and Management',1),(34,13,'HND Office Technology and Management',1),(35,14,'ND Computer Engineering',1),(36,14,'HND Computer Engineering',1),(37,15,'ND Civil Engineering',1),(38,15,'HND Civil Engineering (Structures)',1),(39,16,'ND Electrical Electronics Engineering',1),(40,16,'ND PT  Electrical Electronics Engineering',1),(41,16,'HND Elect.  Engr. (Electronics & Telecom)',1),(42,16,'HND Elect. Engr. (Power & Machine)',1),(43,17,'ND Mechanical Engineering',1),(44,18,'ND Architectural Technology ',1),(45,18,'HND Architectural Technology ',1),(46,19,'ND Building Technology',1),(47,19,'ND PT',1),(48,19,'HND Building Technology',1),(49,20,'ND Estate Management',1),(50,20,'ND PT',1),(51,20,'HND Estate Management',1),(52,21,'ND Survey and Geoinformatics',1);

/*Table structure for table `registrations` */

DROP TABLE IF EXISTS `registrations`;

CREATE TABLE `registrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reg_number` varchar(30) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `school_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `programme_id` int(11) DEFAULT NULL,
  `gender` varchar(2) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `nationality_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `home_town` varchar(128) DEFAULT NULL,
  `localgov` varchar(128) DEFAULT NULL,
  `marital_status` varchar(10) DEFAULT NULL,
  `religion_id` int(11) DEFAULT NULL,
  `home_address` varchar(256) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `registrations` */

insert  into `registrations`(`id`,`reg_number`,`student_id`,`school_id`,`department_id`,`programme_id`,`gender`,`dob`,`nationality_id`,`state_id`,`home_town`,`localgov`,`marital_status`,`religion_id`,`home_address`,`date_time`,`status`) values (9,'2014/2015/PT-7E591/9',9,2,12,31,'M','2015-06-10',1,30,'Software Engineering Lab,Centre of Excellence in software engineering, Obafemi Awolowo University, Ile-Ife, Osun State. Nigeria',NULL,'SINGLE',1,'Software Engineering Lab,Centre of Excellence in software engineering, Obafemi Awolowo University, Ile-Ife, Osun State. Nigeria','2015-06-15 05:36:00',1),(10,'2014/2015/PT-16',NULL,NULL,NULL,NULL,'M','2015-06-24',1,30,'Software Engineering Lab,Centre of Excellence in software engineering, Obafemi Awolowo University, Ile-Ife, Osun State. Nigeria',NULL,'SINGLE',1,'Software Engineering Lab,Centre of Excellence in software engineering, Obafemi Awolowo University, Ile-Ife, Osun State. Nigeria','2015-06-14 09:37:00',1),(11,'2014/2015/PT-C2',NULL,NULL,NULL,NULL,'M','2015-06-24',1,30,'Software Engineering Lab,Centre of Excellence in software engineering, Obafemi Awolowo University, Ile-Ife, Osun State. Nigeria',NULL,'SINGLE',1,'Software Engineering Lab,Centre of Excellence in software engineering, Obafemi Awolowo University, Ile-Ife, Osun State. Nigeria','2015-06-14 09:32:00',1),(12,'2014/2015/PT-85',NULL,NULL,NULL,NULL,'M','2015-06-24',1,30,'Software Engineering Lab,Centre of Excellence in software engineering, Obafemi Awolowo University, Ile-Ife, Osun State. Nigeria',NULL,'SINGLE',1,'Software Engineering Lab,Centre of Excellence in software engineering, Obafemi Awolowo University, Ile-Ife, Osun State. Nigeria','2015-06-14 09:38:00',1),(13,'2014/2015/PT-EE',NULL,NULL,NULL,NULL,'M','2015-06-24',1,30,'Software Engineering Lab,Centre of Excellence in software engineering, Obafemi Awolowo University, Ile-Ife, Osun State. Nigeria',NULL,'SINGLE',1,'Software Engineering Lab,Centre of Excellence in software engineering, Obafemi Awolowo University, Ile-Ife, Osun State. Nigeria','2015-06-14 09:59:00',1),(14,'2014/2015/PT-44F07/',NULL,NULL,NULL,NULL,'M','2015-06-05',1,30,'Software Engineering Lab,Centre of Excellence in software engineering, Obafemi Awolowo University, Ile-Ife, Osun State. Nigeria',NULL,'MARRIED',1,'Software Engineering Lab,Centre of Excellence in software engineering, Obafemi Awolowo University, Ile-Ife, Osun State. Nigeria','2015-06-15 12:22:00',1),(15,'2014/2015/PT-67725/',NULL,NULL,NULL,NULL,'M','2015-06-18',1,30,'Software Engineering Lab,Centre of Excellence in software engineering, Obafemi Awolowo University, Ile-Ife, Osun State. Nigeria',NULL,'DIVORCED',1,'Software Engineering Lab,Centre of Excellence in software engineering, Obafemi Awolowo University, Ile-Ife, Osun State. Nigeria','2015-06-15 12:43:00',1);

/*Table structure for table `relationships` */

DROP TABLE IF EXISTS `relationships`;

CREATE TABLE `relationships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `relationships` */

insert  into `relationships`(`id`,`name`,`status`) values (1,'FATHER',1),(2,'MOTHER',1),(3,'BLOOD SISTER',1),(4,'BLOOD BROTHER',1),(5,'UNCLE',1),(6,'AUNTY',1);

/*Table structure for table `religions` */

DROP TABLE IF EXISTS `religions`;

CREATE TABLE `religions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `religions` */

insert  into `religions`(`id`,`name`,`status`) values (1,'CHRISTIANITY',1),(2,'ISLAM',1);

/*Table structure for table `resultbreakdowns` */

DROP TABLE IF EXISTS `resultbreakdowns`;

CREATE TABLE `resultbreakdowns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `result_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `grade_id` int(11) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;

/*Data for the table `resultbreakdowns` */

insert  into `resultbreakdowns`(`id`,`result_id`,`subject_id`,`grade_id`,`date_time`,`status`) values (82,10,1,1,'2015-06-15 05:36:00',1),(83,10,2,3,'2015-06-15 05:36:00',1),(84,10,3,3,'2015-06-15 05:36:00',1),(85,10,4,6,'2015-06-15 05:36:00',1),(86,10,5,2,'2015-06-15 05:36:00',1),(87,10,6,2,'2015-06-15 05:36:00',1),(88,10,7,3,'2015-06-15 05:36:00',1),(89,10,8,2,'2015-06-15 05:36:00',1),(90,10,4,3,'2015-06-15 05:36:00',1),(91,11,1,1,'2015-06-15 05:37:00',1),(92,11,2,3,'2015-06-15 05:37:00',1),(93,11,3,6,'2015-06-15 05:37:00',1),(94,11,5,6,'2015-06-15 05:37:00',1),(95,11,0,0,'2015-06-15 05:37:00',1),(96,11,0,0,'2015-06-15 05:37:00',1),(97,11,0,0,'2015-06-15 05:37:00',1),(98,11,0,0,'2015-06-15 05:37:00',1),(99,11,0,0,'2015-06-15 05:37:00',1);

/*Table structure for table `results` */

DROP TABLE IF EXISTS `results`;

CREATE TABLE `results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `exam_id` int(11) DEFAULT NULL,
  `exam_no` varchar(15) DEFAULT NULL,
  `exam_date` datetime DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `results` */

insert  into `results`(`id`,`student_id`,`exam_id`,`exam_no`,`exam_date`,`date_time`,`status`) values (10,9,1,'23456787654','2015-06-24 00:00:00','2015-06-15 05:36:00',1),(11,9,2,'4567898765','2015-06-25 00:00:00','2015-06-15 05:36:00',1);

/*Table structure for table `schools` */

DROP TABLE IF EXISTS `schools`;

CREATE TABLE `schools` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `schools` */

insert  into `schools`(`id`,`name`,`status`) values (1,'SCHOOL OF APPLIED SCIENCES',1),(2,'SCHOOL OF BUSINESS STUDIES',1),(3,'SCHOOL OF ENGINEERING TECHNOLOGY',1),(4,'SCHOOL OF ENVIRONMENTAL TECHNOLOGY',1);

/*Table structure for table `sponsors` */

DROP TABLE IF EXISTS `sponsors`;

CREATE TABLE `sponsors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `address` text,
  `phone` varchar(20) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `sponsors` */

insert  into `sponsors`(`id`,`student_id`,`name`,`address`,`phone`,`date_time`,`status`) values (6,9,'Akinboyewa Akindolani','Software Engineering Lab,Centre of Excellence in software engineering, Obafemi Awolowo University, Ile-Ife, Osun State. Nigeria',NULL,'2015-06-15 05:37:00',1);

/*Table structure for table `states` */

DROP TABLE IF EXISTS `states`;

CREATE TABLE `states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

/*Data for the table `states` */

insert  into `states`(`id`,`name`,`status`) values (1,'Abia',1),(2,'Adamawa',1),(3,'Akwa Ibom',1),(4,'Anambra',1),(5,'Bauchi',1),(6,'Bayelsa',1),(7,'Benue',1),(8,'Borno',1),(9,'Cross River',1),(10,'Delta',1),(11,'Ebonyi',1),(12,'Edo',1),(13,'Ekiti',1),(14,'Enugu',1),(15,'FCT',1),(16,'Gombe',1),(17,'Imo',1),(18,'Jigawa',1),(19,'Kaduna',1),(20,'Kano',1),(21,'Katsina',1),(22,'Kebbi',1),(23,'Kogi',1),(24,'Kwara',1),(25,'Lagos',1),(26,'Nasarawa',1),(27,'Niger',1),(28,'Ogun',1),(29,'Ondo',1),(30,'Osun',1),(31,'Oyo',1),(32,'Plateau',1),(33,'Rivers',1),(34,'Sokoto',1),(35,'Taraba',1),(36,'Yobe',1),(37,'Zamfara',1),(38,'Others',1);

/*Table structure for table `students` */

DROP TABLE IF EXISTS `students`;

CREATE TABLE `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(128) DEFAULT NULL,
  `middlename` varchar(128) DEFAULT NULL,
  `lastname` varchar(128) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `token_key` varchar(10) DEFAULT NULL,
  `trans_ref` varchar(10) DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `bank_ref` varchar(128) DEFAULT NULL,
  `payment_status` tinyint(4) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `amount` double(12,2) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `students` */

insert  into `students`(`id`,`firstname`,`middlename`,`lastname`,`email`,`password`,`phone`,`token_key`,`trans_ref`,`payment_date`,`bank_ref`,`payment_status`,`date_time`,`amount`,`status`) values (1,'Adekunle Ojo','Ajayi','Kunle','boye@sanwo.me',NULL,'08099536787','3931a86528',NULL,'2015-06-05 10:49:00',NULL,1,'2015-06-05 10:49:00',10000.00,1),(2,'Adewole Toyin adewale 7','Ajayi','Kunle','wapicadmin@wapic.com',NULL,'08099536787','6e82397d1d',NULL,'2015-06-05 06:36:00',NULL,1,'2015-06-05 06:36:00',10000.00,1),(3,'Adewole Toyin adewale 7','Ajayi','Kunle','admin@fox.com',NULL,'08099536787','3f55260b31','32496ceb26','2015-06-05 07:04:00',NULL,1,'2015-06-05 07:04:00',10000.00,1),(4,'Adekunle Ojo Dami','Ajayi Kayode','Kunle','admin@fox.com',NULL,'08076543455','db402d40d2','d27f123691','2015-06-05 07:31:00',NULL,1,'2015-06-05 07:31:00',10000.00,1),(5,'Adewole Toyin adewale 74','Ajayi Kayode','Kunle','gobc@registration.com',NULL,'08099536787','2B0F246A5D','92E2A914E5','2015-06-06 06:33:00',NULL,4,'2015-06-06 05:29:00',10000.00,1),(6,'Adewole Toyin adewale 7','Ajayi Kayode','Kunle','wapicadmin@wapic.com',NULL,'08099536787','128509539F','A9B4C7E769','2015-06-06 06:16:00',NULL,1,'2015-06-06 06:16:00',10000.00,1),(7,'Adekunle Ojo Dami','Ajayi Kayode','Kunle','wapicadmin@wapic.com',NULL,'5454565','4F539F7C1B','A26E9666D4','2015-06-06 06:52:00',NULL,4,'2015-06-06 06:45:00',10000.00,1),(8,'Adewole Toyin adewale 74','Ajayi Kayode','Kunle','wapicadmin@wapic.com',NULL,'08099536787','5CB15FE261','21FA60D49E','2015-06-06 07:25:00',NULL,2,'2015-06-06 07:26:00',10000.00,1),(9,'Adewole Toyin adewale 7','Ajayi Kayode','Kunle','wapicadmin@wapic.com',NULL,'08043567654','177354356C','C43F3049DB','2015-06-06 07:16:00',NULL,3,'2015-06-06 07:41:00',10000.00,2);

/*Table structure for table `subjects` */

DROP TABLE IF EXISTS `subjects`;

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `subjects` */

insert  into `subjects`(`id`,`name`,`status`) values (1,'MATHEMATICS',1),(2,'ENGLISH',1),(3,'CHEMISTRY',1),(4,'BIOLOGY',1),(5,'PHYSICS',1),(6,'ECONOMICS',1),(7,'COMMERCE',1),(8,'ACCOUNTING',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
